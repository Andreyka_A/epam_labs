﻿using Caliburn.Micro;
using EpamProject.TestPerformance.Models;
using EpamProject.TestPerformance.Services;
using System;
using System.Threading.Tasks;

namespace EpamProject.TestPerformance.ViewModels
{
    public class ResultViewModel : PropertyChangedBase
    {
        private TestService _testService;

        public ResultViewModel(Settings settings)
        {
            Result = new Result();
            Result.Settings = settings;
            
            _testService = new TestService(Result);

            _testService.OnResultsUpdate += ResultsUpdated;
            Parallel.For(0, Result.Settings.AmountThreads, (i) => _testService.Run());
        }

        public void Save()
        {
            _testService.Save();
        }

        public void CancelTest()
        {
            _testService.CancelTest();
        }

        private void ResultsUpdated(object sender, EventArgs args)
        {
            NotifyOfPropertyChange(() => Result);
        }

        public Result Result { get; set; }

        public Settings Settings
        {
            get { return Result.Settings; }
            set
            {
                Result.Settings = value;
                NotifyOfPropertyChange(() => Settings);
            }
        }
    }
}