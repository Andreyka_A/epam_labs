﻿using Caliburn.Micro;
using EpamProject.TestPerformance.Models;

namespace EpamProject.TestPerformance.ViewModels
{
    public class MainViewModel : PropertyChangedBase
    {
        private Settings _settings;

        public MainViewModel()
        {
            _settings = new Settings {
                AmountRequests = 6,
                AmountThreads = 3,
                Duration = 4000,
                Url = "http://stackoverflow.com"
            };
        }

        public Settings Settings
        {
            get { return _settings; }
            set
            {
                _settings = value;
                NotifyOfPropertyChange(() => Settings);
            }
        }

        public void RunTest()
        {
            IWindowManager manager = new WindowManager();
            manager.ShowDialog(new ResultViewModel(_settings), null, null);
        }
    }
}
