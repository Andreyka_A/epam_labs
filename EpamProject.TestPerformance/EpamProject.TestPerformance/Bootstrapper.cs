﻿using Caliburn.Micro;
using EpamProject.TestPerformance.ViewModels;
using System.Windows;

namespace EpamProject.TestPerformance
{
    public class Bootstrapper : BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainViewModel>();
        }

        protected override void Configure()
        {
            base.Configure();
        }
    }
}
