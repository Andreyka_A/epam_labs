﻿using Caliburn.Micro;
using EpamProject.TestPerformance.Models;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using OxyPlot.Series;
using System.Timers;
using System.Net.Http;
using Microsoft.Win32;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace EpamProject.TestPerformance.Services
{
    public class TestService : PropertyChangedBase
    {
        public event EventHandler OnResultsUpdate;

        public Result Result { get; set; }

        private bool IsCancel { get; set; }
        private bool IsComplete { get; set; }

        private System.Timers.Timer _timer;
        private CancellationTokenSource _cancellationTokenSource;
        private List<Result> _resultsPerSeconds;
        private AreaSeries _totalRequestsAreaSeries;
        private AreaSeries _successfulRequestsAreaSeries;
        private AreaSeries _failedRequestsAreaSeries;
        private double _stepPogressBar;
        private long _sumResponce;
        private int _currentSec;


        public TestService(Result result)
        {
            Result = result;
            Result.Graph = new PlotModel();
            _cancellationTokenSource = new CancellationTokenSource();
            _timer = new System.Timers.Timer(1000);
            _timer.Elapsed += UpdateData;
            _resultsPerSeconds = new List<Result>();
            _stepPogressBar = 100.0 / (Result.Settings.AmountRequests * Result.Settings.AmountThreads);
            _sumResponce = 0;
            _currentSec = 0;

            _totalRequestsAreaSeries = new AreaSeries
            {
                Title = "Total requests per second",
                Color = OxyColor.FromRgb(48, 193, 150)
            };
            _successfulRequestsAreaSeries = new AreaSeries
            {
                Title = "Successful requests per second",
                Color = OxyColor.FromRgb(0, 102, 204)
            };
            _failedRequestsAreaSeries = new AreaSeries
            {
                Title = "Failed requests per second",
                Color = OxyColor.FromRgb(199, 69, 69)
            };

            Result.Graph.Series.Add(_totalRequestsAreaSeries);
            Result.Graph.Series.Add(_successfulRequestsAreaSeries);
            Result.Graph.Series.Add(_failedRequestsAreaSeries);
            Result.Graph.InvalidatePlot(true);
        }

        public void ResultsUpdate()
        {
            OnResultsUpdate?.Invoke(this, EventArgs.Empty);
        }

        private void UpdateGraph()
        {
            _totalRequestsAreaSeries.Points.Add(new DataPoint(_currentSec, Result.TotalRequestsPerSecond));
            _successfulRequestsAreaSeries.Points.Add(new DataPoint(_currentSec, Result.SuccessfulRequestsPerSecond));
            _failedRequestsAreaSeries.Points.Add(new DataPoint(_currentSec, Result.FailedRequestsPerSecond));
            Result.Graph.InvalidatePlot(true);
        }

        private void UpdateData(object sender, ElapsedEventArgs e)
        {
            if (Result.TotalRequests == Result.Settings.AmountRequests * Result.Settings.AmountThreads)
            {
                _cancellationTokenSource.Cancel();
                UpdateGraph();
                _timer.Stop();

                ShowMessage();
                return;
            }

            if (_timer.Enabled && !IsCancel)
            {
                UpdateGraph();
                _currentSec++;

                Result.TotalRequestsPerSecond = 0;
                Result.TotalRequestsPerSecond = 0;
                Result.FailedRequestsPerSecond = 0;
                Result.SuccessfulRequestsPerSecond = 0;
            }
        }

        private void ShowMessage()
        {
            if (!IsComplete)
            {
                IsComplete = true;
                UpdateGraph();

                if (IsCancel)
                    Execute.OnUIThread(() => MessageBox.Show("Test canceled or time is over", "Test", MessageBoxButton.OK, MessageBoxImage.Information));
                else
                {
                    Result.ProgressBar = 100;
                    Execute.OnUIThread(() => MessageBox.Show("Test complete", "Test", MessageBoxButton.OK, MessageBoxImage.Asterisk));
                }
            }
        }

        public void Run()
        {
            try
            {
                Task[] tasks = Enumerable.Range(0, Result.Settings.AmountRequests).Select(arg => Task.Run(() => SendRequests(_cancellationTokenSource.Token))).ToArray();
                _cancellationTokenSource.CancelAfter(Result.Settings.Duration);

                Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                CancelTest();
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async void SendRequests(CancellationToken token)
        {
            if (token.IsCancellationRequested || IsCancel)
                return;

            ResultsUpdate();

            using (var client = new HttpClient())
            {
                try
                {
                    _timer.Enabled = true;
                    token.ThrowIfCancellationRequested();

                    Stopwatch stopWatch = Stopwatch.StartNew();
                    var responce = await client.GetAsync(Result.Settings.Url, token);
                    stopWatch.Stop();

                    Result.ProgressBar += _stepPogressBar;
                    Result.Bandwidth = (long)responce.Content.Headers.ContentLength / stopWatch.ElapsedMilliseconds;

                    if (Result.MinResponse == 0 && Result.MaxResponse == 0)
                    {
                        Result.MinResponse = stopWatch.ElapsedMilliseconds;
                        Result.MaxResponse = stopWatch.ElapsedMilliseconds;
                    }
                    else
                    {
                        Result.MinResponse = Result.MinResponse < stopWatch.ElapsedMilliseconds ? Result.MinResponse : stopWatch.ElapsedMilliseconds;
                        Result.MaxResponse = Result.MaxResponse > stopWatch.ElapsedMilliseconds ? Result.MaxResponse : stopWatch.ElapsedMilliseconds;
                    }

                    _sumResponce += stopWatch.ElapsedMilliseconds;

                    if (_sumResponce != 0 && Result.TotalRequests != 0)
                        Result.AverageResponse = _sumResponce / Result.TotalRequests;

                    Result.SuccessfulRequestsPerSecond++;
                }

                catch (Exception)
                {
                    Execute.OnUIThread(() =>
                    {
                        Result.TotalErrors++;
                        Result.FailedRequestsPerSecond++;
                    });

                    IsCancel = true;
                    ShowMessage();
                }

                Execute.OnUIThread(() =>
                {
                    Result.TotalRequests++;
                    Result.TotalRequestsPerSecond++;
                });
            }

            ResultsUpdate();
        }

        public void CancelTest()
        {
            if (!IsCancel && _timer.Enabled)
            {
                IsCancel = true;
                _cancellationTokenSource.Cancel();

                ShowMessage();
            }
        }

        public void Save()
        {
            if (!IsComplete)
                return;

            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Text File (*.pdf)|*.pdf|Show All Files (*.*)|*.*";
            saveDialog.FileName = "Untitled";
            saveDialog.Title = "Save As";

            if (saveDialog.ShowDialog() == true)
            {
                using (FileStream fs = new FileStream(saveDialog.FileName, FileMode.Create, FileAccess.Write))
                {
                    Document doc = new Document(iTextSharp.text.PageSize.A4, 25, 25, 30, 30);
                    PdfWriter writter = PdfWriter.GetInstance(doc, fs);

                    doc.Open();

                    doc.Add(new Phrase("Settings", FontFactory.GetFont(FontFactory.DefaultEncoding, 13, Font.NORMAL, BaseColor.GRAY)));
                    doc.Add(new Paragraph($"Amount requests: {Result.Settings.AmountRequests}"));
                    doc.Add(new Paragraph($"Amount threads: {Result.Settings.AmountThreads}"));
                    doc.Add(new Paragraph($"Duration: {Result.Settings.Duration}"));
                    doc.Add(new Paragraph($"Url: {Result.Settings.Url}"));

                    doc.Add(new Phrase("Results", FontFactory.GetFont(FontFactory.DefaultEncoding, 13, Font.NORMAL, BaseColor.GRAY)));
                    doc.Add(new Paragraph($"Total requests: {Result.TotalRequests}"));
                    doc.Add(new Paragraph($"Total errrors: {Result.TotalErrors}"));
                    doc.Add(new Paragraph($"Bandwidth: {Result.Bandwidth}"));
                    doc.Add(new Paragraph($"Min response: {Result.MinResponse}"));
                    doc.Add(new Paragraph($"Max response: {Result.MaxResponse}"));
                    doc.Add(new Paragraph($"Average response: {Result.AverageResponse}"));

                    doc.Close();
                }
            }
        }
    }
}
