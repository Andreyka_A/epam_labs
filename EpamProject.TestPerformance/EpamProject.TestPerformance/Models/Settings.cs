﻿namespace EpamProject.TestPerformance.Models
{
    public class Settings
    {
        public int AmountThreads { get; set; }
        public int AmountRequests { get; set; }
        public string Url { get; set; }
        public int Duration { get; set; }
    }
}
