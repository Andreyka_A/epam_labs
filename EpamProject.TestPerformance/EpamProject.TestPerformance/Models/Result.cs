﻿using OxyPlot;

namespace EpamProject.TestPerformance.Models
{
    public class Result
    {
        public Settings Settings { get; set; }
        public PlotModel Graph { get; set; }
        public double ProgressBar { get; set; }
        public double TotalRequestsPerSecond { get; set; }
        public double SuccessfulRequestsPerSecond { get; set; }
        public double FailedRequestsPerSecond { get; set; }
        public int TotalRequests { get; set; }
        public long Bandwidth { get; set; }
        public int TotalErrors { get; set; }
        public long MinResponse { get; set; }
        public long MaxResponse { get; set; }
        public long AverageResponse { get; set; }
    }
}
