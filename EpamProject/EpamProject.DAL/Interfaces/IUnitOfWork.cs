﻿using EpamProject.DAL.Entities;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace EpamProject.DAL.Interfaces
{
    public interface IUnitOfWork 
        : IDisposable
    {
        IRepository<Category> Categories { get; }
        IRepository<Event> Events { get; }
        IRepository<Session> Sessions { get; }
        IRepository<Order> Orders { get; }
        IRepository<Record> Records { get; }
        IRepository<Ticket> Tickets { get; }
        IRepository<City> Cities { get; }
        IRepository<Venue> Venues { get; }
        IEnumerable<ApplicationUser> Users { get; }
        IEnumerable<IdentityRole> Roles { get; }
        void SaveChanges();
    }
}
