﻿using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        void Add(T model);
        void AddRange(params T[] entities);
        void Remove(T model);
        void Update(T model);
        T GetById(int id);
    }
}
