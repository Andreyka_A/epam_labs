﻿namespace EpamProject.DAL.Entities.Services
{
    public interface IUKey<Key>
    {
        Key Id { get; set; }
    }
}
