﻿namespace EpamProject.DAL.Entities.Enums
{
    public enum TicketState
    {
        // стадии
        Open, // открытие
        Expectation, // ожидание
        InTransit, // если почтой, то в пути, а уже затем, получено
        Received, // получено
        Closed // закрыт
    }
}
