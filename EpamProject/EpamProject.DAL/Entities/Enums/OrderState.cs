﻿namespace EpamProject.DAL.Entities.Enums
{
    public enum OrderState
    {
        // стадии
        Expectation, // ожидание
        InTransit, // если почтой, то подтверждено
        Received, // иначе, получено
        Rejected, // отклонен
        Closed // закрыт
    }
}
