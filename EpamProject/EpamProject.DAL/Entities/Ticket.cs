﻿using EpamProject.DAL.Entities.Enums;
using EpamProject.DAL.Entities.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EpamProject.DAL.Entities
{
    public class Ticket : IUKey<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string SaleDescription { get; set; }

        public string Comment { get; set; }
        public TicketState State { get; set; }

        public string TrackingNumber { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateCompletion { get; set; }
        public double Price { get; set; }
        public bool IsShow { get; set; }
        public string PhoneNumber { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public int SessionId { get; set; }
        public Session Session { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
