﻿using EpamProject.DAL.Entities.Services;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EpamProject.DAL.Entities
{
    public class Category : IUKey<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }

        public int EventId { get; set; }
        public Event Event { get; set; }

        public ICollection<Record> Records { get; set; }
    }
}
