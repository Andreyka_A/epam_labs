﻿using EpamProject.DAL.Entities.Enums;
using EpamProject.DAL.Entities.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EpamProject.DAL.Entities
{
    public class Order : IUKey<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string CommentFromSeller { get; set; }
        public OrderState State { get; set; }
        public PurchaseMethod PurchaseMethod { get; set; }
        public string TrackingNumber { get; set; }
        public bool IsShow { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Comment { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateCompletion { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public int TicketId { get; set; }
        public Ticket Ticket { get; set; }
    }
}
