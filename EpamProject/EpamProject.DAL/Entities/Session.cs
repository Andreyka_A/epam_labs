﻿using EpamProject.DAL.Entities.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EpamProject.DAL.Entities
{
    public class Session : IUKey<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Poster { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }

        public int RecordId { get; set; }
        public Record Record { get; set; }

        public int VenueId { get; set; }
        public Venue Venue { get; set; }

        public ICollection<Ticket> Tickets { get; set; }
    }
}
