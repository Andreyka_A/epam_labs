﻿using System.Collections.Generic;
using EpamProject.DAL.Entities;
using EpamProject.DAL.EF;
using EpamProject.DAL.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;

namespace EpamProject.DAL.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DomainModelContext _context;
        private bool _disposed;

        private CategoryRepository _categories;
        private EventRepository _events;
        private SessionRepository _sessions;
        private OrderRepository _orders;
        private RecordRepository _records;
        private TicketRepository _tickets;
        private CityRepository _cities;
        private VenueRepository _venues;

        public UnitOfWork(DomainModelContext context)
        {
            _context = context;
            _disposed = false;
        }

        public IRepository<Category> Categories
        {
            get
            {
                if (_categories == null)
                    _categories = new CategoryRepository(_context);
                return _categories;
            }
        }

        public IRepository<Event> Events
        {
            get
            {
                if (_events == null)
                    _events = new EventRepository(_context);
                return _events;
            }
        }

        public IRepository<Session> Sessions
        {
            get
            {
                if (_sessions == null)
                    _sessions = new SessionRepository(_context);
                return _sessions;
            }
        }

        public IRepository<Order> Orders
        {
            get
            {
                if (_orders == null)
                    _orders = new OrderRepository(_context);
                return _orders;
            }
        }

        public IRepository<Record> Records
        {
            get
            {
                if (_records == null)
                    _records = new RecordRepository(_context);
                return _records;
            }
        }

        public IRepository<Ticket> Tickets
        {
            get
            {
                if (_tickets == null)
                    _tickets = new TicketRepository(_context);
                return _tickets;
            }
        }

        public IRepository<City> Cities
        {
            get
            {
                if (_cities == null)
                    _cities = new CityRepository(_context);
                return _cities;
            }
        }

        public IRepository<Venue> Venues
        {
            get
            {
                if (_venues == null)
                    _venues = new VenueRepository(_context);
                return _venues;
            }
        }

        public IEnumerable<ApplicationUser> Users
        {
            get { return _context.Users; }
        }

        public IEnumerable<IdentityRole> Roles
        {
            get { return _context.Roles; }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if(disposing)
                    _context.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}