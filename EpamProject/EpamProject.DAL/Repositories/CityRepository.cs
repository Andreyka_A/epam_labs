﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class CityRepository : IRepository<City>
    {
        private readonly DomainModelContext _context;

        public CityRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(City model)
        {
            _context.Cities.Add(model);
        }

        public void AddRange(params City[] entities)
        {
            _context.Cities.AddRange(entities);
        }

        public void Remove(City model)
        {
            _context.Cities.Remove(model);
        }

        public IQueryable<City> GetAll()
        {
            return _context.Cities
                .Include(v => v.Venues)
                    .ThenInclude(c => c.City);
        }

        public City GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(City model)
        {
            _context.Cities.Update(model);
        }
    }
}
