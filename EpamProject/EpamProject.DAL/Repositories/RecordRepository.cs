﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class RecordRepository : IRepository<Record>
    {
        private readonly DomainModelContext _context;

        public RecordRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Record model)
        {
            _context.Records.Add(model);
        }

        public void AddRange(params Record[] entities)
        {
            _context.Records.AddRange(entities);
        }

        public void Remove(Record model)
        {
            _context.Records.Remove(model);
        }

        public IQueryable<Record> GetAll()
        {
            return _context.Records
                .Include(c => c.Category)
                    .ThenInclude(e => e.Event)
                .Include(s => s.Sessions)
                    .ThenInclude(r => r.Record);
        }

        public Record GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Record model)
        {
            _context.Records.Update(model);
        }
    }
}
