﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class SessionRepository : IRepository<Session>
    {
        private readonly DomainModelContext _context;

        public SessionRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Session model)
        {
            _context.Sessions.Add(model);
        }

        public void AddRange(params Session[] entities)
        {
            _context.Sessions.AddRange(entities);
        }

        public void Remove(Session model)
        {
            _context.Sessions.Remove(model);
        }

        public IQueryable<Session> GetAll()
        {
            return _context.Sessions
                .Include(t => t.Tickets)
                .Include(v => v.Venue)
                    .ThenInclude(c => c.City)
                .Include(r => r.Record)
                    .ThenInclude(c => c.Category)
                        .ThenInclude(e => e.Event);
        }

        public Session GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Session model)
        {
            _context.Sessions.Update(model);
        }
    }
}
