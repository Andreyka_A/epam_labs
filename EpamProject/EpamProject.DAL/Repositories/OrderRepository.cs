﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        private readonly DomainModelContext _context;

        public OrderRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Order model)
        {
            _context.Orders.Add(model);
        }

        public void AddRange(params Order[] entities)
        {
            _context.Orders.AddRange(entities);
        }

        public void Remove(Order model)
        {
            _context.Orders.Remove(model);
        }

        public IQueryable<Order> GetAll()
        {
            return _context.Orders
                .Include(u => u.ApplicationUser)
                .Include(t => t.Ticket.ApplicationUser)
                .Include(t => t.Ticket)
                    .ThenInclude(o => o.Session)
                        .ThenInclude(r => r.Record)
                            .ThenInclude(c => c.Category)
                                .ThenInclude(e => e.Event)
                .Include(t => t.Ticket.Session.Venue)
                    .ThenInclude(c => c.City);
        }

        public Order GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Order model)
        {
            _context.Orders.Update(model);
        }
    }
}
