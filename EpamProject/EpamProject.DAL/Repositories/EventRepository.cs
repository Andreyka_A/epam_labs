﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class EventRepository : IRepository<Event>
    {
        private readonly DomainModelContext _context;

        public EventRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Event model)
        {
            _context.Events.Add(model);
        }

        public void AddRange(params Event[] entities)
        {
            _context.Events.AddRange(entities);
        }

        public void Remove(Event model)
        {
            _context.Events.Remove(model);
        }

        public IQueryable<Event> GetAll()
        {
            return _context.Events
                .Include(c => c.Categories)
                    .ThenInclude(r => r.Records);
        }

        public Event GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Event model)
        {
            _context.Events.Update(model);
        }
    }
}
