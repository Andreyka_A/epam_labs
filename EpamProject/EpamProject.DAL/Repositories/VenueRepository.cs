﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class VenueRepository : IRepository<Venue>
    {
        private readonly DomainModelContext _context;

        public VenueRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Venue model)
        {
            _context.Venues.Add(model);
        }

        public void AddRange(params Venue[] entities)
        {
            _context.Venues.AddRange(entities);
        }

        public void Remove(Venue model)
        {
            _context.Venues.Remove(model);
        }

        public IQueryable<Venue> GetAll()
        {
            return _context.Venues
                .Include(c => c.City)
                    .ThenInclude(v => v.Venues)
                .Include(s => s.Sessions)
                    .ThenInclude(v => v.Record)
                        .ThenInclude(c => c.Category)
                            .ThenInclude(e => e.Event);
        }

        public Venue GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Venue model)
        {
            _context.Venues.Update(model);
        }
    }
}
