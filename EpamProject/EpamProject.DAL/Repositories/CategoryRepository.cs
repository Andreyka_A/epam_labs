﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class CategoryRepository : IRepository<Category>
    {
        private readonly DomainModelContext _context;

        public CategoryRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Category model)
        {
            _context.Categories.Add(model);
        }

        public void AddRange(params Category[] entities)
        {
            _context.Categories.AddRange(entities);
        }

        public void Remove(Category model)
        {
            _context.Categories.Remove(model);
        }

        public IQueryable<Category> GetAll()
        {
            return _context.Categories.Include(e => e.Event);
        }

        public Category GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Category model)
        {
            _context.Categories.Update(model);
        }
    }
}
