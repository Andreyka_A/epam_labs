﻿using EpamProject.DAL.EF;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.DAL.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {
        private readonly DomainModelContext _context;

        public TicketRepository(DomainModelContext context)
        {
            _context = context;
        }

        public void Add(Ticket model)
        {
            _context.Tickets.Add(model);
        }

        public void AddRange(params Ticket[] entities)
        {
            _context.Tickets.AddRange(entities);
        }

        public void Remove(Ticket model)
        {
            _context.Tickets.Remove(model);
        }

        public IQueryable<Ticket> GetAll()
        {
            return _context.Tickets
                .Include(u => u.ApplicationUser)
                .Include(or => or.Orders)
                    .ThenInclude(t => t.Ticket)
                .Include(or => or.Orders)
                    .ThenInclude(u => u.ApplicationUser)
                .Include(s => s.Session)
                    .ThenInclude(r => r.Record)
                        .ThenInclude(c => c.Category)
                            .ThenInclude(e => e.Event)
                .Include(s => s.Session.Venue)
                    .ThenInclude(c => c.City);
        }

        public Ticket GetById(int id)
        {
            return GetAll().FirstOrDefault(m => m.Id == id);
        }

        public void Update(Ticket model)
        {
            _context.Tickets.Update(model);
        }
    }
}
