﻿using EpamProject.DAL.EF;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using EpamProject.DAL.Entities.Enums;

namespace EpamProject.WEB.Initializer
{
    public static class DadabaseInitialize
    {
        public static void Seed(this IApplicationBuilder app)
        {
            DomainModelContext _context = app.ApplicationServices.GetService<DomainModelContext>();

            if (_context.Roles.Any())
                return;

            var hasher = new PasswordHasher<ApplicationUser>();

            #region Roles
            _context.Roles.AddRange(
                new IdentityRole { Name = "Administrator", NormalizedName = "ADMINISTRATOR" },
                new IdentityRole { Name = "Moderator", NormalizedName = "MODERATOR" },
                new IdentityRole { Name = "User", NormalizedName = "USER" });
            _context.SaveChanges();

            var rAdmin = _context.Roles.Where(p => p.Name == "Administrator").FirstOrDefault();
            var rModer = _context.Roles.Where(p => p.Name == "Moderator").FirstOrDefault();
            var rUser = _context.Roles.Where(p => p.Name == "User").FirstOrDefault();
            #endregion

            #region Users
            var uAdmin = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Admin",
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                NormalizedUserName = "ADMIN",
                Email = "admin@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM",
                Localization = "en",
                SecurityStamp = Guid.NewGuid().ToString(),
                LockoutEnabled = true
            };
            uAdmin.PasswordHash = hasher.HashPassword(uAdmin, "Admin");

            var uModer = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Moder",
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                NormalizedUserName = "MODER",
                Email = "moder@gmail.com",
                NormalizedEmail = "MODER@GMAIL.COM",
                Localization = "ru",
                SecurityStamp = Guid.NewGuid().ToString(),
                LockoutEnabled = true
            };
            uModer.PasswordHash = hasher.HashPassword(uModer, "Moder");

            var uUser = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "User",
                FirstName = "TestFirstName",
                LastName = "TestLastName",
                NormalizedUserName = "USER",
                Email = "user@gmail.com",
                NormalizedEmail = "USER@GMAIL.COM",
                Localization = "be",
                SecurityStamp = Guid.NewGuid().ToString(),
                LockoutEnabled = true
            };
            uUser.PasswordHash = hasher.HashPassword(uUser, "User");

            
            _context.Users.AddRange(uAdmin, uModer, uUser);
            _context.SaveChanges();
            #endregion

            #region UserRoles
            _context.UserRoles.AddRange(
                new IdentityUserRole<string> { RoleId = rAdmin.Id, UserId = uAdmin.Id },
                new IdentityUserRole<string> { RoleId = rModer.Id, UserId = uModer.Id },
                new IdentityUserRole<string> { RoleId = rUser.Id, UserId = uUser.Id }
                );

            _context.SaveChanges();
            #endregion

            #region Events
            Event sport = new Event { Name = "Sport" }; // 1
            Event concerts = new Event { Name = "Concerts" }; // 2
            Event movies = new Event { Name = "Movies" }; // 3
            Event theater = new Event { Name = "Theater" }; // 4

            _context.Events.AddRange(new List<Event> { sport, concerts, movies, theater });
            _context.SaveChanges();
            #endregion

            #region Categories
            Category basketball = new Category // 1
            {
                Name = "Basketball",
                EventId = sport.Id
            };
            Category football = new Category // 2
            {
                Name = "Football",
                EventId = sport.Id
            };
            Category hockey = new Category // 3
            {
                Name = "Hockey",
                EventId = sport.Id
            };

            // -

            Category rock = new Category // 4
            {
                Name = "Rock",
                EventId = concerts.Id
            };
            Category pop = new Category // 5
            {
                Name = "Pop",
                EventId = concerts.Id
            };

            Category rap = new Category // 6
            {
                Name = "Rap",
                EventId = concerts.Id
            };

            // -

            Category comedy = new Category // 7
            {
                Name = "Comedy",
                EventId = movies.Id
            };
            Category drama = new Category // 8
            {
                Name = "Drama",
                EventId = movies.Id
            };

            Category thriller = new Category // 9
            {
                Name = "Thriller",
                EventId = movies.Id
            };

            // ---

            Category tragedy = new Category // 10
            {
                Name = "Tragedy",
                EventId = theater.Id
            };

            _context.Categories.AddRange(
                basketball, football, hockey,
                rock, pop, rap,
                comedy, drama, thriller,
                tragedy);

            _context.SaveChanges();
            #endregion

            #region Cities
            City gomel = new City // 1
            {
                Name = "Gomel"
            };

            City minsk = new City // 2
            {
                Name = "Minsk"
            };

            City boston = new City // 3
            {
                Name = "Boston"
            };

            City newYork = new City // 4
            {
                Name = "New York"
            };

            City miami = new City // 5
            {
                Name = "Miami"
            };

            _context.Cities.AddRange(
                gomel, minsk, boston,
                newYork, miami);

            _context.SaveChanges();
            #endregion

            #region Venues
            Venue makaenka10 = new Venue // 1
            {
                Address = "Makaenka 10",
                CityId = 1 // gomel
            };

            Venue sviridova3 = new Venue // 2
            {
                Address = "Sviridova 3",
                CityId = 2 // minsk
            };

            Venue checherskaya1 = new Venue // 3
            {
                Address = "Checherskaya 1",
                CityId = 1 // gomel
            };

            Venue beaconStreetGuestHouse = new Venue // 4
            {
                Address = "463 Beacon Street Guest House",
                CityId = 3 // boston
            };

            Venue allenStreet5 = new Venue // 5
            {
                Address = "231 Allen Street 5",
                CityId = 4 // new york
            };

            Venue evergladesNationalPark = new Venue // 6
            {
                Address = "204th Ave - Everglades National Park",
                CityId = 5 // maimi
            };

            _context.Venues.AddRange(
                makaenka10, sviridova3, checherskaya1,
                beaconStreetGuestHouse, allenStreet5, evergladesNationalPark);

            _context.SaveChanges();
            #endregion

            #region Records
            Record gomel_minsk = new Record // 1
            {
                Name = "Gomel - Minsk",
                Description = "Some description Gomel - Minsk",
                Poster = "ae8f7395-0326-4d0f-88c8-43f65f52378d.jpg",
                CategoryId = 3 // hockey.Id
            };

            Record grodno_gomel = new Record // 2
            {
                Name = "Grodno - Gomel",
                Description = "Some description Grodno - Gomel",
                Poster = "6ceee7d7-5648-4abd-bac1-60526019e5c0.jpg",
                CategoryId = 3 // hockey.Id
            };

            Record boston_detroit = new Record // 3
            {
                Name = "Boston - Detroit",
                Description = "Some description Boston - Detroit",
                Poster = "08cd15f3-d0a3-4a41-899f-bab9b77d4d8a.jpg",
                CategoryId = 1 // basketball.Id
            };

            Record metallica = new Record // 4
            {
                Name = "Metallica",
                Description = "Some description Metallica",
                Poster = "d36f914d-079e-4ab0-b472-4de4b6052eee.jpg",
                CategoryId = 4 // rock.Id
            };

            Record one_plus_one = new Record // 5
            {
                Name = "1+1",
                Description = "Some description The 1+1",
                Poster = "caabaec4-7dde-4ddb-8532-b5d3a715b30f.jpg",
                CategoryId = 8 // drama.Id
            };

            _context.Records.AddRange(gomel_minsk, grodno_gomel, boston_detroit, metallica, one_plus_one);
            _context.SaveChanges();
            #endregion

            #region Offers
            Session gomel_minsk_of = new Session // 1
            {
                VenueId = 1,
                Date = new DateTime(2017, 10, 15, 19, 10, 0),
                Description = "Some disc for Gomel - Minsk",
                Poster = "54392081-8724-4b4e-832d-dd45e2405c1f.jpg",
                RecordId = 1 // gomel_minsk.Id
            };

            Session gomel_minsk_of2 = new Session // 2
            {
                VenueId = 2,
                Date = new DateTime(2017, 12, 3, 18, 30, 0),
                Description = "Some disc for Gomel - Minsk",
                Poster = "7396ecdb-bbd9-456f-be11-2f870c62ccac.jpg",
                RecordId = 1 // gomel_minsk.Id
            };

            Session grodno_gomel_of = new Session // 3
            {
                VenueId = 3,
                Date = new DateTime(2017, 5, 10, 17, 30, 0),
                Description = "Some disc for Grodno - Gomel",
                Poster = "c757ce5a-3b18-4cd7-a0b5-4f5fb3135a65.jpg",
                RecordId = 2 // grodno_gomel.Id
            };

            Session boston_detroit_of2 = new Session // 4
            {
                VenueId = 4,
                Date = new DateTime(2017, 7, 11, 17, 30, 0),
                Description = "Some disc for Boston - Detroit",
                Poster = "c02793a6-9550-4f92-93b5-4fac07c46d2c.jpg",
                RecordId = 3 // boston_detroit.Id
            };

            // -

            Session metallica_of = new Session // 5
            {
                VenueId = 5,
                Date = new DateTime(2017, 8, 5, 15, 0, 0),
                Description = "Some disc for Metallica",
                Poster = "af624b8e-903b-46b5-ac88-2c40b58ce00c.jpg",
                RecordId = 4 // metallica.Id
            };

            // -
            Session one_plus_one_of = new Session // 6
            {
                VenueId = 6,
                Date = new DateTime(2017, 11, 3, 17, 50, 0),
                Description = "Some disc for 1+1",
                Poster = "8299a182-bd28-430c-868c-075731a6a3ca.jpg",
                RecordId = 5 // one_plus_one.Id
            };

            _context.Sessions.AddRange(gomel_minsk_of, gomel_minsk_of2, grodno_gomel_of, boston_detroit_of2, metallica_of, one_plus_one_of);
            _context.SaveChanges();
            #endregion

            #region Tickets
            Ticket gomel_minsk_t = new Ticket // 1
            {
                ApplicationUserId = uAdmin.Id,
                DateCreate = new DateTime(2016, 12, 5, 15, 30, 0),
                Price = 10,
                SaleDescription = "I sell a ticket gomel minsk",
                SessionId = 1, // gomel_minsk_of.Id
                Comment = String.Empty,
                TrackingNumber = "1412412adadw",
                State = TicketState.Expectation,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            Ticket gomel_minsk_t2 = new Ticket // 2
            {
                ApplicationUserId = uUser.Id,
                DateCreate = new DateTime(2016, 12, 6, 13, 30, 0),
                Price = 13,
                SaleDescription = "I sell a ticket gomel minsk 2",
                SessionId = 1, // gomel_minsk_of.Id
                Comment = String.Empty,
                TrackingNumber = "141241adwa12adadw",
                State = TicketState.Expectation,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            Ticket metallica_t = new Ticket // 3
            {
                ApplicationUserId = uModer.Id,
                DateCreate = new DateTime(2016, 5, 7, 10, 13, 0),
                Price = 100,
                SaleDescription = "I sell a ticket 1 + 1",
                SessionId = 5, // metallica_of.Id
                Comment = String.Empty,
                TrackingNumber = "1awdw1412412adadw",
                State = TicketState.Expectation,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            Ticket one_plus_one_t = new Ticket // 4
            {
                ApplicationUserId = uUser.Id,
                DateCreate = new DateTime(2016, 6, 10, 11, 13, 0),
                Price = 3,
                SaleDescription = "I sell a ticket 1 + 1 123",
                SessionId = 6, // one_plus_one_of.Id
                Comment = String.Empty,
                TrackingNumber = "14124awd112adadw",
                State = TicketState.Expectation,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            Ticket one_plus_one_t2 = new Ticket // 5
            {
                ApplicationUserId = uAdmin.Id,
                DateCreate = new DateTime(2016, 7, 11, 12, 11, 0),
                Price = 4,
                SaleDescription = "I sell a ticket 1 + 1 345",
                SessionId = 6, //one_plus_one_of.Id
                Comment = String.Empty,
                TrackingNumber = "awdawd11412412adadw",
                State = TicketState.Expectation,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            _context.Tickets.AddRange(gomel_minsk_t, gomel_minsk_t2, metallica_t, one_plus_one_t, one_plus_one_t2);
            _context.SaveChanges();
            #endregion

            #region Orders
            Order order1 = new Order // 1
            {
                ApplicationUserId = uUser.Id,
                TicketId = 1, // gomel_minsk_t.Id
                TrackingNumber = "121512512323",
                DateCreate = new DateTime(2017, 1, 2, 5, 15, 0),
                Comment = String.Empty,
                CommentFromSeller = String.Empty,
                State = OrderState.Expectation,
                PurchaseMethod = PurchaseMethod.Mail,
                IsShow = true,
                PhoneNumber = "+375441234567",
                Address = "Gomel, Makaenka 10/10"
            };

            Order order2 = new Order // 2
            {
                ApplicationUserId = uUser.Id,
                TicketId = 3, // metallica_t.Id
                TrackingNumber = "121adw3223",
                DateCreate = new DateTime(2017, 2, 10, 3, 12, 30),
                Comment = String.Empty,
                CommentFromSeller = String.Empty,
                State = OrderState.Expectation,
                PurchaseMethod = PurchaseMethod.Personally,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            Order order3 = new Order // 3
            {
                ApplicationUserId = uAdmin.Id,
                TicketId = 4, // one_plus_one_t.Id
                TrackingNumber = "1215awd125123123",
                DateCreate = new DateTime(2017, 1, 12, 7, 3, 15),
                Comment = String.Empty,
                CommentFromSeller = String.Empty,
                State = OrderState.Expectation,
                PurchaseMethod = PurchaseMethod.Personally,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            Order order4 = new Order // 4
            {
                ApplicationUserId = uModer.Id,
                TicketId = 5, // one_plus_one_t2.Id
                TrackingNumber = "a12123awadawd",
                DateCreate = new DateTime(2017, 4, 1, 3, 9, 0),
                Comment = String.Empty,
                CommentFromSeller = String.Empty,
                State = OrderState.Expectation,
                PurchaseMethod = PurchaseMethod.Mail,
                IsShow = true,
                PhoneNumber = "+375441234567",
                Address = "Brest, Makaenka 25/25"
            };

            Order order5 = new Order // 5
            {
                ApplicationUserId = uModer.Id,
                TicketId = 2, // gomel_minsk_t2.Id
                TrackingNumber = "gwad2314fdsa",
                DateCreate = new DateTime(2017, 2, 9, 8, 7, 10),
                Comment = String.Empty,
                CommentFromSeller = String.Empty,
                State = OrderState.Expectation,
                PurchaseMethod = PurchaseMethod.Personally,
                IsShow = true,
                PhoneNumber = "+375441234567"
            };

            _context.Orders.AddRange(order1, order2, order3, order4, order5 );
            _context.SaveChanges();
            #endregion
        }
    }
}
