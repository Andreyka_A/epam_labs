﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using EpamProject.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using EpamProject.WEB.Localization;
using Microsoft.Extensions.Options;
using EpamProject.DAL.EF;
using Microsoft.EntityFrameworkCore;
using EpamProject.DAL.Interfaces;
using EpamProject.DAL.Repositories;
using EpamProject.BLL.Interfaces;
using EpamProject.BLL.Services;
using EpamProject.WEB.Initializer;
using Sakura.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Swagger;
using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL.API.Services;
using Newtonsoft.Json;
using EpamProject.WEB.Formatters;
using Microsoft.Net.Http.Headers;
using EpamProject.WEB.Services;

namespace EpamProject.WEB
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddDbContext<DomainModelContext>(options =>
              options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("EpamProject.WEB")));

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 2;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
            });

            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IEventService, EventService>();
            services.AddTransient<ISessionService, SessionService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IRecordService, RecordService>();
            services.AddTransient<ITicketService, TicketService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IVenueService, VenueService>();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<DomainModelContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<IApiCategoryService, ApiCategoryService>();
            services.AddTransient<IApiEventService, ApiEventService>();
            services.AddTransient<IApiCityService, ApiCityService>();
            services.AddTransient<IApiRecordService, ApiRecordService>();
            services.AddTransient<IApiVenueService, ApiVenueService>();
            services.AddTransient<IApiSessionService, ApiSessionService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Title = "My API",
                    Version = "v1"
                });

                c.OperationFilter<AcceptParameter>();
            });

            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });

            var csvFormatterOptions = new CsvFormatterOptions();

            services.AddMvc(options => {
                options.OutputFormatters.Add(new CsvOutputFormatter(csvFormatterOptions));
                options.FormatterMappings.SetMediaTypeMappingForFormat("csv", MediaTypeHeaderValue.Parse("text/csv"));
            })
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .AddXmlSerializerFormatters();
            //.AddXmlDataContractSerializerFormatters();

            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                new CultureInfo("en"),
                new CultureInfo("ru"),
                new CultureInfo("be")
            };

                options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;

                options.RequestCultureProviders.Insert(0, new RouteCultureProvider(options.DefaultRequestCulture));
            });

            services.AddBootstrapPagerGenerator(options =>
            {
                options.ConfigureDefault();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // Db initialize
            applicationLifetime.ApplicationStarted.Register(app.Seed);

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseSwagger();

            app.UseSwaggerUi(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseIdentity();

            var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(localizationOptions.Value);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "InternationalizationDefault",
                    template: "{culture=en}/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}