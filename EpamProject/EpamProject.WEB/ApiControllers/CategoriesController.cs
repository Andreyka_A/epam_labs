﻿using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.API.Interfaces;
using System.Linq;

namespace EpamProject.WEB.ApiControllers
{
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private readonly IApiCategoryService _apiCategoryService;

        public CategoriesController(
            IApiCategoryService apiCategoryService)
        {
            _apiCategoryService = apiCategoryService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var data = _apiCategoryService.GetAll().ToList();

            return new ObjectResult(data);
        }
    }
}
