﻿using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL;
using System.Linq;

namespace EpamProject.WEB.ApiControllers
{
    [Route("api/[controller]")]
    public class RecordsController : Controller
    {
        private readonly IApiRecordService _apiRecordService;

        public RecordsController(
            IApiRecordService apiRecordService)
        {
            _apiRecordService = apiRecordService;
        }

        [HttpGet]
        public IActionResult Get([FromQuery]string sValue)
        {
            var filter = _apiRecordService.GetNameRecordsToSearch(sValue, Constants.LimitOutputSearch).ToList();

            return new ObjectResult(filter);
        }
    }
}