﻿using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.API.Interfaces;
using System.Linq;

namespace EpamProject.WEB.ApiControllers
{
    [Route("api/[controller]")]
    public class EventsController : Controller
    {
        private readonly IApiEventService _apiEventService;

        public EventsController(
            IApiEventService apiEventService)
        {
            _apiEventService = apiEventService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var data = _apiEventService.GetAll().ToList();

            return new ObjectResult(data);
        }
        
        [HttpGet]
        [Route("GetCategories")]
        public IActionResult Get([FromQuery]int eventId)
        {
            var data = _apiEventService.GetCategories(eventId).ToList();

            return new ObjectResult(data);
        }
    }
}
