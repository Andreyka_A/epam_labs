﻿using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.API.Interfaces;
using System.Linq;

namespace EpamProject.WEB.ApiControllers
{
    [Route("api/[controller]")]
    public class CitiesController : Controller
    {
        private readonly IApiCityService _apiCityService;

        public CitiesController(
            IApiCityService apiCityService)
        {
            _apiCityService = apiCityService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var data = _apiCityService.GetAll().ToList();

            return new ObjectResult(data);
        }

        [HttpGet]
        [Route("GetVenues")]
        public IActionResult Get([FromQuery]int cityId)
        {
            var data = _apiCityService.GetVenues(cityId).ToList();

            return new ObjectResult(data);
        }
    }
}
