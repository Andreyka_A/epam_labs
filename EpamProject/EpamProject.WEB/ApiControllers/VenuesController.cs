﻿using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.API.Interfaces;
using System.Linq;

namespace EpamProject.WEB.ApiControllers
{
    [Route("api/[controller]")]
    public class VenuesController : Controller
    {
        private readonly IApiVenueService _apiVenueService;

        public VenuesController(
            IApiVenueService apiVenueService)
        {
            _apiVenueService = apiVenueService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var data = _apiVenueService.GetAll().ToList();

            return new ObjectResult(data);
        }
    }
}
