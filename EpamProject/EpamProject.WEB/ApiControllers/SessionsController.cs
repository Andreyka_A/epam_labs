﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.API.Interfaces;
using EpamProject.WEB.Services;
using System.Linq;

namespace EpamProject.WEB.ApiControllers
{
    [Route("api/[controller]")]
    public class SessionsController : Controller
    {
        private readonly IApiSessionService _apiSessionService;

        public SessionsController(
            IApiSessionService apiSessionService)
        {
            _apiSessionService = apiSessionService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var sessions = _apiSessionService.GetAll();
            sessions = _apiSessionService.SortDesc(sessions, "DATE").ToList();

            return new ObjectResult(CustomHelpers.ToPagedList(Response, sessions));
        }

        [HttpGet]
        [Route("Find")]
        public IActionResult Get(
            [FromQuery]string recordName, 
            [FromQuery]DateTime dateFrom,
            [FromQuery]DateTime dateTo, 
            [FromQuery]List<int> citiesIds,
            [FromQuery]List<int> venuesIds,
            [FromQuery]string sort = "DESC",
            [FromQuery]string orderBy = "DATE",
            [FromQuery]int page = 0)
        {
            var sessions = _apiSessionService.GetAll();

            if(!string.IsNullOrEmpty(recordName))
                sessions = _apiSessionService.FindByRecordName(sessions, recordName);

            if ((dateFrom != null && dateFrom != DateTime.MinValue) && (dateTo != null && dateTo != DateTime.MinValue))
                sessions = _apiSessionService.FindByDateRange(sessions, dateFrom, dateTo);
            else
            {
                if (dateFrom != null && dateFrom != DateTime.MinValue)
                    sessions = _apiSessionService.FindByDateFrom(sessions, dateFrom);

                if (dateTo != null && dateTo != DateTime.MinValue)
                    sessions = _apiSessionService.FindByDateTo(sessions, dateTo);
            }

            if (citiesIds.Count != 0)
                sessions = _apiSessionService.FindByCitiesIds(sessions, citiesIds);

            if (venuesIds.Count != 0)
                sessions = _apiSessionService.FindByVenuesIds(sessions, venuesIds);

            if(!String.IsNullOrEmpty(sort) && !String.IsNullOrEmpty(orderBy))
            {
                if (sort.ToUpper() == "DESC")
                    sessions = _apiSessionService.SortDesc(sessions, orderBy);
                else
                    sessions = _apiSessionService.Sort(sessions, orderBy);
            }

            return new ObjectResult(CustomHelpers.ToPagedList(Response, sessions.ToList(), page));
        }
    }
}
