﻿using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.ManageViewModels
{
    public class ProfileChangeDataViewModel
    {
        [Required]
        [Display(Name = "First Name")]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(30, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string LastName { get; set; }

        [Display(Name = "About Me")]
        [MaxLength(15, ErrorMessage = "The maximum record length {0} characters.")]
        [MinLength(3, ErrorMessage = "The minimum record length {0} characters.")]
        public string AboutMe { get; set; }
    }
}
