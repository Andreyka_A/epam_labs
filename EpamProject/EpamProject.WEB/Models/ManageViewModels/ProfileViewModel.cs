﻿using EpamProject.DAL.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.ManageViewModels
{
    public class ProfileViewModel
    {
        public ApplicationUser User { get; set; }
        public int TotalTickets { get; set; }
        public int ActiveTickets { get; set; }
        public int SoldTickets { get; set; }

        public string RoleId { get; set; }
        public SelectList Roles { get; set; }

        public ProfileChangeDataViewModel ProfileChangeData { get; set; }
        public ProfileChangePasswordViewModel ProfileChangePassword { get; set; }
    }
}
