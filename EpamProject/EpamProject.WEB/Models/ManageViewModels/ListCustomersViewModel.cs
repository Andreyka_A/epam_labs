﻿using EpamProject.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.ManageViewModels
{
    public class ListCustomersViewModel
    {
        public Ticket Ticket { get; set; }
        public int PageSize { get; set; }
        public int Page { get; set; }

        [Required]
        public int OrderId { get; set; }

        [StringLength(30, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string TrackingNumber { get; set; }

        [StringLength(300, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
        public string CommentFromSeller { get; set; }
    }
}
