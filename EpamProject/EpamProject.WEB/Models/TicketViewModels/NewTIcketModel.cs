﻿using EpamProject.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.TicketViewModels
{
    public class NewTicketModel
    {
        public Session Session { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string Description { get; set; }

        [Required]
        public double Price { get; set; }

        [Required]
        public int SessionId { get; set; }
    }
}
