﻿using EpamProject.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.TicketViewModels
{
    public class EditTicketModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string SaleDescription { get; set; }

        [Required]
        public double Price { get; set; }


        [Required]
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        [Required]
        public int SessionId { get; set; }
        public Session Session { get; set; }
    }
}
