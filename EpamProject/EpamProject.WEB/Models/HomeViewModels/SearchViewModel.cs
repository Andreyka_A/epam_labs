﻿using EpamProject.DAL.Entities;
using Sakura.AspNetCore;

namespace EpamProject.WEB.Models.HomeViewModels
{
    public class SearchViewModel
    {
        public string TopSearch { get; set; }
        public IPagedList<Record> Records { get; set; }
    }
}
