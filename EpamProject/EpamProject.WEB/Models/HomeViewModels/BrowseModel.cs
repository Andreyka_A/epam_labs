﻿using EpamProject.DAL.Entities;
using Sakura.AspNetCore;
using System.Collections.Generic;

namespace EpamProject.WEB.Models.HomeViewModels
{
    public class BrowseModel
    {
        public IPagedList<Record> Records { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}
