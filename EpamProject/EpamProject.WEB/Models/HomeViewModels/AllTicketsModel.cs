﻿using EpamProject.DAL.Entities;
using Sakura.AspNetCore;

namespace EpamProject.WEB.Models.HomeViewModels
{
    public class AllTicketsModel
    {
        public IPagedList<Ticket> Tickets { get; set; }
        public Session Session { get; set; }
    }
}
