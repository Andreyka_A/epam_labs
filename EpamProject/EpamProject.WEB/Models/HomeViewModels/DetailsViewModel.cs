﻿using EpamProject.DAL.Entities;
using EpamProject.DAL.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.HomeViewModels
{
    public class DetailsViewModel
    {
        public Ticket Ticket { get; set; }

        [Required]
        public PurchaseMethod PurchaseMethod { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(300, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string Address { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(300, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        public string Comment { get; set; }
    }
}
