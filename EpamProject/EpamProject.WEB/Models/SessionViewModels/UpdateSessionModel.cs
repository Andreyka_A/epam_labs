﻿using EpamProject.DAL.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.SessionViewModels
{
    public class UpdateSessionModel
    {
        //[Required(ErrorMessage = "Please Upload a Valid Image File. Only jpg format allowed")]
        [DataType(DataType.Upload)]
        [FileExtensions(Extensions = "jpg")]
        public IFormFile Poster;

        [Required]
        [StringLength(500, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 5)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required]
        public int CityId { get; set; }

        [Required]
        public int VenueId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        public int RecordId { get; set; }
    }
}
