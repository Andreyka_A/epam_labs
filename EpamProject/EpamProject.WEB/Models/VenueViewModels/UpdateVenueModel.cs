﻿using EpamProject.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace EpamProject.WEB.Models.VenueViewModels
{
    public class UpdateVenueModel
    {
        [Required]
        public int Id { get; set; }

        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string Address { get; set; }

        public City City { get; set; }
    }
}
