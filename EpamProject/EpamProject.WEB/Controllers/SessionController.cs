﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using EpamProject.WEB.Models.SessionViewModels;
using EpamProject.DAL.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using EpamProject.BLL.Interfaces;
using System.IO;

namespace EpamProject.WEB.Controllers
{
    [Authorize(Roles = "Administrator, Moderator")]
    public class SessionController : Controller
    {
        IHostingEnvironment _appEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ICategoryService _categoryService;
        private readonly IEventService _eventService;
        private readonly ISessionService _sessionService;
        private readonly IOrderService _orderService;
        private readonly IRecordService _recordService;
        private readonly ITicketService _ticketService;
        private readonly ICityService _cityService;
        private readonly IVenueService _venueService;

        public SessionController(
            IHostingEnvironment appEnvironment, 
            ICategoryService categoryService, 
            IEventService eventService, 
            ISessionService sessionService,
            IOrderService orderService, 
            IRecordService recordService, 
            ITicketService ticketService,
            ICityService cityService,
            IVenueService venueService,
             UserManager<ApplicationUser> userManager, 
             SignInManager<ApplicationUser> signInManager)
        {
            _appEnvironment = appEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
            _categoryService = categoryService;
            _eventService = eventService;
            _sessionService = sessionService;
            _orderService = orderService;
            _recordService = recordService;
            _ticketService = ticketService;
            _cityService = cityService;
            _venueService = venueService;
        }

        [HttpGet]
        public IActionResult AddSession(int recordId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _sessionService.GetById(recordId);
            if (data == null)
                return NotFound();

            var model = new UpdateSessionModel()
            {
                RecordId = recordId
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddSession(IFormFile Poster, UpdateSessionModel model)
        {
            if (ModelState.IsValid)
            {
                var newSession = new Session();

                var currentCity = _cityService.GetById(model.CityId);
                if (currentCity == null)
                    return NotFound();

                var currentVenue = _venueService.GetById(model.VenueId);
                if (currentVenue == null)
                    return NotFound();

                if (!currentCity.Venues.Contains(currentVenue))
                    return NotFound();

                var data = _sessionService.GetById(model.RecordId);
                if (data == null)
                    return NotFound();

                if (Poster != null && Poster.Length > 0)
                {
                    var file = Poster;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "images\\Posters");

                    if (file.Length > 0)
                    {
                        var fileName = Guid.NewGuid().ToString() + ".jpg";
                        var path = Path.Combine(uploads, fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            file.CopyToAsync(fileStream);
                            newSession.VenueId = model.VenueId;
                            newSession.Date = model.Date;
                            newSession.Description = model.Description;
                            newSession.Poster = fileName;
                            newSession.RecordId = model.RecordId;
                        }
                    }
                }

                _sessionService.Create(newSession);
                return RedirectToAction("item", "home", new { recordId = newSession.RecordId });
            }
            else
            {
                return View("addsession", model);
            }
        }

        [HttpGet]
        public IActionResult EditSession(int sessionId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _sessionService.GetById(sessionId);
            if (data == null)
                return NotFound();

            var model = new UpdateSessionModel
            {
                RecordId = data.RecordId,
                VenueId = data.VenueId,
                Date = data.Date,
                Description = data.Description
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditSession(IFormFile Poster, UpdateSessionModel model)
        {
            if (ModelState.IsValid)
            {
                var data = _sessionService.GetById(model.RecordId);
                if (data == null)
                    return NotFound();

                var currentCity = _cityService.GetById(model.CityId);
                if (currentCity == null)
                    return NotFound();

                var currentVenue = _venueService.GetById(model.VenueId);
                if (currentVenue == null)
                    return NotFound();

                if (!currentCity.Venues.Contains(currentVenue))
                    return NotFound();

                if (Poster != null && Poster.Length > 0)
                {
                    var file = Poster;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "images\\Posters");

                    if (file.Length > 0)
                    {
                        var fileName = Guid.NewGuid().ToString() + ".jpg";
                        var path = Path.Combine(uploads, fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            file.CopyToAsync(fileStream);
                            data.Poster = fileName;
                        }
                    }
                }

                data.VenueId = model.VenueId;
                data.Date = model.Date;
                data.RecordId = model.RecordId;
                data.Description = model.Description;

                _sessionService.Update(data);
                return RedirectToAction("item", "home", new { recordId = data.RecordId });
            }
            else
            {
                return View("editsession", model);
            }
        }

        [HttpGet]
        public IActionResult RemoveSession(int sessionId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _sessionService.GetById(sessionId);
            if (data == null)
                return NotFound();

            if (data.Tickets == null)
            {
                _sessionService.Remove(data);
                return RedirectToAction("item", "home", new { recordId = data.RecordId });
            }
            else if (data.Tickets.Count == 0)
            {
                _sessionService.Remove(data);
                return RedirectToAction("item", "home", new { recordId = data.RecordId });
            }
            else
                return NotFound("Deleting is not possible, there are tickets for this session");
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
