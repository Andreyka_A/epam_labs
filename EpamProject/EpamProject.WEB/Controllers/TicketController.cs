using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using EpamProject.BLL.Interfaces;
using EpamProject.WEB.Models.TicketViewModels;
using Microsoft.AspNetCore.Authorization;
using EpamProject.DAL.Entities.Enums;
using EpamProject.WEB.Models.ManageViewModels;
using System.Linq;

namespace EpamProject.WEB.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        private readonly IStringLocalizer<AccountController> _localizer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;
        private readonly ISessionService _sessionService;
        private readonly IOrderService _orderService;
        private readonly ITicketService _ticketService;

        public TicketController(
           ISessionService sessionService,
           IOrderService orderService,
           ITicketService ticketService,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           RoleManager<IdentityRole> roleManager,
           ILoggerFactory loggerFactory,
           IStringLocalizer<AccountController> localizer)
        {
            _localizer = localizer;
            _sessionService = sessionService;
            _orderService = orderService;
            _ticketService = ticketService;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }

        [HttpGet]
        public IActionResult Create(int sessionId)
        {
            var session = _sessionService.GetById(sessionId);
            if (session == null)
                return NotFound();

            var model = new NewTicketModel
            {
                Session = session,
                SessionId = sessionId
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(NewTicketModel model)
        {
            if (model == null)
                return NotFound();

            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var session = _sessionService.GetById(model.SessionId);
            if (session == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                Ticket ticket = new Ticket
                {
                    ApplicationUserId = user.Result.Id,
                    DateCreate = DateTime.Now,
                    SessionId = session.Id,
                    Price = model.Price,
                    SaleDescription = model.Description,
                    State = TicketState.Open,
                    IsShow = true
                };

                _ticketService.Create(ticket);
                var currentTicket = _ticketService.GetLastByUserId(user.Result.Id);

                return RedirectToAction("Details", "Home", new { ticketId = currentTicket.Id });
            }

            var newTicketModel = new NewTicketModel
            {
                Session = session,
                SessionId = model.SessionId,
                Description = model.Description,
                Price = model.Price
            };

            return View(newTicketModel);
        }

        [HttpGet]
        public IActionResult Edit(int ticketId)
        {
            var ticket = _ticketService.GetById(ticketId);
            if (ticket == null)
                return NotFound();

            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            if (user.Result.Id != ticket.ApplicationUserId)
                return NotFound();

            EditTicketModel model = new EditTicketModel
            {
                Id = ticket.Id,
                ApplicationUserId = ticket.ApplicationUserId,
                ApplicationUser = ticket.ApplicationUser,
                SessionId = ticket.SessionId,
                Session = ticket.Session,
                Price = ticket.Price,
                SaleDescription = ticket.SaleDescription,
            };

            if (ticket.State == TicketState.Open && ticket.IsShow)
                return View(model);
            else
                return NotFound("The operation is not possible. Your ticket is in the standby state, or it has not yet delivered.");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(EditTicketModel model)
        {
            if (model == null)
                return NotFound();

            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var ticket = _ticketService.GetById(model.Id);
            if (ticket == null || ticket.State != TicketState.Open)
                return NotFound();

            if (user.Result.Id != ticket.ApplicationUserId)
                return NotFound();

            if (ModelState.IsValid)
            {
                ticket.SaleDescription = model.SaleDescription;
                ticket.Price = model.Price;

                if (ticket.State == TicketState.Open && ticket.IsShow)
                {
                    _ticketService.Update(ticket);
                    return RedirectToAction("Details", "Home", new { ticketId = ticket.Id });
                }
                else
                    return NotFound("The operation is not possible. Your ticket is in the standby state, or it has not yet delivered.");
            }

            EditTicketModel editTicketModel = new EditTicketModel
            {
                Id = ticket.Id,
                ApplicationUserId = ticket.ApplicationUserId,
                ApplicationUser = ticket.ApplicationUser,
                SessionId = ticket.SessionId,
                Session = ticket.Session,
                Price = ticket.Price,
                SaleDescription = ticket.SaleDescription,
            };

            return View(editTicketModel);
        }

        public IActionResult NotRelevant(int ticketId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var ticket = _ticketService.GetById(ticketId);
            if (ticket == null)
                return NotFound();

            if (ticket.ApplicationUserId != user.Result.Id)
                return NotFound();

            if(ticket.State == TicketState.Open && ticket.IsShow)
            {
                _ticketService.Close(ticket);
                return RedirectToAction("mytickets", "manage");
            }
            else
                return NotFound("The operation is not possible. Your ticket is in the standby state, or it has not yet delivered.");
        }

        public IActionResult Clear(int ticketId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var ticket = _ticketService.GetById(ticketId);
            if (ticket == null)
                return NotFound();

            if (ticket.ApplicationUserId != user.Result.Id)
                return NotFound();

            if((ticket.State == TicketState.Closed || ticket.State == TicketState.Received) && ticket.IsShow)
            {
                _ticketService.Clear(ticket);
                return RedirectToAction("mytickets", "manage");
            }
            else
                return NotFound("The operation is not possible. Your ticket is in the standby state, or it has not yet delivered.");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SellTicket(ListCustomersViewModel model)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var order = _orderService.GetById(model.OrderId);
            if (order == null)
                return NotFound();

            var ticket = _ticketService.GetById(order.TicketId);
            if (ticket == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                if (order.PurchaseMethod == PurchaseMethod.Mail && String.IsNullOrEmpty(model.TrackingNumber))
                {
                    ModelState.AddModelError("TrackingNumber", "The TrackingNumber field is required.");
                    model.Ticket = ticket;
                    return View("../manage/listcustomers", model);
                }

                if (order.PurchaseMethod == PurchaseMethod.Mail)
                {
                    order.State = OrderState.InTransit;
                    ticket.State = TicketState.InTransit;
                }

                if (order.PurchaseMethod == PurchaseMethod.Personally)
                {
                    order.DateCompletion = DateTime.Now;
                    order.State = OrderState.Received;

                    ticket.DateCompletion = DateTime.Now;
                    ticket.State = TicketState.Received;
                }

                order.CommentFromSeller = model.CommentFromSeller;
                order.TrackingNumber = model.TrackingNumber;
                
                ticket.Orders
                    .Where(x => x.ApplicationUserId != order.ApplicationUserId)
                    .Select(x => { x.State = OrderState.Rejected; x.CommentFromSeller = "Ticket sold"; return x; })
                    .ToList();

                _orderService.Update(order);
                _ticketService.Update(ticket);
                return RedirectToAction("mytickets", "manage");
            }

            model.Ticket = ticket;
            return View($"../manage/ListCustomers/ticketId={ticket.Id}", model);
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
    }
}