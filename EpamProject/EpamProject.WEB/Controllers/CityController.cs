﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using EpamProject.BLL.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using EpamProject.WEB.Models.CityViewModels;
using EpamProject.BLL;
using Sakura.AspNetCore;

namespace EpamProject.WEB.Controllers
{
    [Authorize(Roles = "Administrator, Moderator")]
    public class CityController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ICityService _cityService;

        public CityController(
           ICityService cityService,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           RoleManager<IdentityRole> roleManager)
        {
            _cityService = cityService;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }

        [HttpGet]
        public IActionResult ListCities(int page = Constants.PageDefault)
        {
            var data = _cityService.GetAll().ToList();
            if (data == null)
                return NotFound();

            var pagedData = data.ToPagedList(Constants.PageSize, page);
            return View(pagedData);
        }

        [HttpGet]
        public IActionResult AddCity()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCity(UpdateCityModel model)
        {
            if (ModelState.IsValid)
            {
                var data = _cityService.GetByName(model.Name);
                if (data != null)
                {
                    ModelState.AddModelError("Name", "This name already exists");
                    return View(model);
                }

                data = new City
                {
                    Name = model.Name
                };
                
                _cityService.Create(data);
                return RedirectToAction("profile", "manage");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult EditCity(int cityId)
        {
            var data = _cityService.GetById(cityId);
            if (data == null)
                return NotFound();

            var model = new UpdateCityModel
            {
                Id = data.Id,
                Name = data.Name
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditCity(UpdateCityModel model)
        {
            if (ModelState.IsValid)
            {
                var data = _cityService.GetById(model.Id);
                if (data == null)
                    return NotFound();

                if (_cityService.GetByName(model.Name) != null)
                {
                    ModelState.AddModelError("Name", "This name already exists");
                    return View(model);
                }

                data.Name = model.Name;

                _cityService.Update(data);
                return RedirectToAction("profile", "manage");
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult RemoveCity(int cityId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _cityService.GetById(cityId); 
            if (data == null)
                return NotFound();

            if (data.Venues == null)
            {
                _cityService.Remove(data);
                return RedirectToAction("listcities", "city");
            }
            else if (data.Venues.Count == 0)
            {
                _cityService.Remove(data);
                return RedirectToAction("listcities", "city");
            }
            else
                return NotFound("Deleting is not possible, there are venues for this city");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
