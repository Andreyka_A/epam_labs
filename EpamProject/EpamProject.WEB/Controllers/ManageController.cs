using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using EpamProject.BLL.Interfaces;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using System.Threading.Tasks;
using EpamProject.WEB.Models.ManageViewModels;
using Sakura.AspNetCore;
using EpamProject.BLL;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EpamProject.WEB.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ICategoryService _categoryService;
        private readonly IEventService _eventService;
        private readonly ISessionService _sessionService;
        private readonly IOrderService _orderService;
        private readonly IRecordService _recordService;
        private readonly ITicketService _ticketService;


        public ManageController(
            ICategoryService categoryService, 
            IEventService eventService, 
            ISessionService sessionService,
            IOrderService orderService, 
            IRecordService recordService, 
            ITicketService ticketService,
            RoleManager<IdentityRole> roleManager, 
            UserManager<ApplicationUser> userManager, 
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _categoryService = categoryService;
            _eventService = eventService;
            _sessionService = sessionService;
            _orderService = orderService;
            _recordService = recordService;
            _ticketService = ticketService;
        }

        public IActionResult MyOrders(int page = Constants.PageDefault)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _orderService.GetOpenOrdersByUserId(user.Result.Id);
            if (data == null)
                return NotFound();

            var pagedData = data.ToPagedList(Constants.PageSize, page);
            return View(pagedData);
        }

        public IActionResult MyTickets(int page = Constants.PageDefault)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _ticketService.GetOpenTicketsByUserId(user.Result.Id);
            if (data == null)
                return NotFound();

            var pagedData = data.ToPagedList(Constants.PageSize, page);
            return View(pagedData);
        }

        public IActionResult DetailsOrder(int orderId)
        {
            var data = _orderService.GetById(orderId);
            if (data == null)
                return NotFound();

            return View(data);
        }

        public IActionResult Profile(string userId = "")
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            if (!String.IsNullOrEmpty(userId))
                user = _userManager.FindByIdAsync(userId);

            var userData = new ProfileChangeDataViewModel
            {
                FirstName = user.Result.FirstName,
                LastName = user.Result.LastName,
                AboutMe = user.Result.AboutMe
            };

            var model = new ProfileViewModel
            {
                User = user.Result,
                ProfileChangeData = userData,
                TotalTickets = _ticketService.GetCountTotalTicketsByUserId(user.Result.Id),
                ActiveTickets = _ticketService.GetCountActiveTicketsByUserId(user.Result.Id),
                SoldTickets = _ticketService.GetCountSoldTicketsByUserId(user.Result.Id),
                Roles = new SelectList(_roleManager.Roles, "Id", "Name")
            };

            return View(model);
        }

        public IActionResult ListCustomers(int ticketId, int page = Constants.PageDefault)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _ticketService.GetActive(ticketId);
            if (data == null)
                return NotFound();

            if (data.ApplicationUserId != user.Result.Id)
                return NotFound();

            return View(new ListCustomersViewModel { Ticket = data, PageSize = Constants.PageSize, Page = page});
        }

        public IActionResult TicketsArchive(int page = Constants.PageDefault)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _ticketService.GetHideTicketsByUserId(user.Result.Id);
            if (data == null)
                return NotFound();

            var pagedData = data.ToPagedList(Constants.PageSize, page);
            return View(pagedData);
        }

        public IActionResult OrdersArchive(int page = Constants.PageDefault)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _orderService.GetHideOrdersByUserId(user.Result.Id);
            if (data == null)
                return NotFound();

            var pagedData = data.ToPagedList(Constants.PageSize, page);
            return View(pagedData);
        }

        public async Task<IActionResult> ChangeData(ProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var curUser = _userManager.FindByIdAsync(model.User.Id);
                if (curUser.Result == null)
                    return NotFound();

                return View("../manage/profile", model);
            }

            var currentUser = GetCurrentUserAsync();
            if (currentUser.Result == null)
                return RedirectToAction("logoff", "account");

            var user = _userManager.FindByIdAsync(model.User.Id);
            if (user.Result == null)
                return NotFound();

            if (currentUser.Result.Id != user.Result.Id)
                return NotFound();

            currentUser.Result.FirstName = model.User.FirstName;
            currentUser.Result.LastName = model.User.LastName;
            currentUser.Result.AboutMe = model.User.AboutMe;

            var result = await _userManager.UpdateAsync(currentUser.Result);
            if (result.Succeeded)
                await _signInManager.RefreshSignInAsync(currentUser.Result);
            else
                AddErrors(result);

            return RedirectToAction("profile", "manage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                var currentUser = _userManager.FindByIdAsync(model.User.Id);
                if (currentUser.Result == null)
                    return NotFound();

                model.User = currentUser.Result;
                return View("profile", model);
            }

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.ChangePasswordAsync(user, model.ProfileChangePassword.OldPassword, model.ProfileChangePassword.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("profile", "manage");
                }
                AddErrors(result);
            }
            return RedirectToAction("profile", "manage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> SetRole(ProfileViewModel model)
        {
            var user = _userManager.FindByIdAsync(model.User.Id);
            if (user.Result == null)
                return NotFound();

            var role = _roleManager.FindByIdAsync(model.RoleId);
            if (role.Result == null)
                return NotFound();

            var roles = _userManager.GetRolesAsync(user.Result).Result;
            if (roles.Contains(role.Result.Name))
                return NotFound("The current user already has this role.");

            await _userManager.RemoveFromRolesAsync(user.Result, roles);
            await _userManager.AddToRoleAsync(user.Result, role.Result.Name);

            return RedirectToAction("profile", "manage", new { userId = user.Result.Id });
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}