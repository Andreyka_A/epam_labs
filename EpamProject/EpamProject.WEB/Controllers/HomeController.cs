﻿using System;
using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.Interfaces;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using EpamProject.WEB.Services;
using EpamProject.WEB.Models.HomeViewModels;
using EpamProject.BLL;
using Sakura.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using System.Linq;

namespace EpamProject.WEB.Controllers
{
    public class HomeController : Controller
    {
        IHostingEnvironment _appEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ICategoryService _categoryService;
        private readonly IEventService _eventService;
        private readonly ISessionService _sessionService;
        private readonly IOrderService _orderService;
        private readonly IRecordService _recordService;
        private readonly ITicketService _ticketService;
        private readonly ICityService _cityService;
        private readonly IVenueService _venueService;

        public HomeController(
            IHostingEnvironment appEnvironment, 
            ICategoryService categoryService, 
            IEventService eventService, 
            ISessionService sessionService,
            IOrderService orderService, 
            IRecordService recordService, 
            ITicketService ticketService, 
            ICityService cityService, 
            IVenueService venueService,
             UserManager<ApplicationUser> userManager, 
             SignInManager<ApplicationUser> signInManager)
        {
            _appEnvironment = appEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
            _categoryService = categoryService;
            _eventService = eventService;
            _sessionService = sessionService;
            _orderService = orderService;
            _recordService = recordService;
            _ticketService = ticketService;
            _cityService = cityService;
            _venueService = venueService;
        }

        public IActionResult Index()
        {
            return View(_eventService.GetAll().ToList());
        }

        public IActionResult Details(int ticketId)
        {
            var model = _ticketService.GetById(ticketId);
            if (model == null)
                return NotFound();

            return View(new DetailsViewModel { Ticket = model });
        }

        public IActionResult AllTickets(int sessionId, int page = Constants.PageDefault)
        {
            var session = _sessionService.GetById(sessionId);
            if (session == null)
                return NotFound();

            var tickets = _ticketService.GetTicketsBySessionId(sessionId);
            if (tickets == null)
                return NotFound();

            var pagedData = tickets.ToPagedList(Constants.PageSize, page);
            return View(new AllTicketsModel { Session = session, Tickets = pagedData });
        }

        public IActionResult Item(int recordId, int page = Constants.PageDefault)
        {
            var record = _recordService.GetById(recordId);
            if (record == null)
                return NotFound();

            var data = _sessionService.GetSessionsByRecordWithProposals(record.Id);
            if (data == null)
                return NotFound();

            data.Page = Constants.PageDefault;
            data.PageSize = Constants.PageSize;
            return View(data);
        }

        public IActionResult Browse(string eventName = "Sport", string categoryName = "", int page = Constants.PageDefault)
        {
            eventName = CustomHelpers.FirstLetterToUpper(eventName);
            ViewBag.Browse = eventName;

            BrowseModel data = new BrowseModel();
            if (!String.IsNullOrEmpty(categoryName))
            {
                data.Records = _recordService.GetRecordsByEvent(eventName, categoryName).ToPagedList(Constants.PageSize, page);
                data.Categories = _categoryService.GetCategoriesByEvent(eventName);
                return View(data);
            }

            data.Records = _recordService.GetRecordsByEvent(eventName).ToPagedList(Constants.PageSize, page);
            data.Categories = _categoryService.GetCategoriesByEvent(eventName);
            return View(data);
        }

        public IActionResult About()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            ApplicationUser user = new ApplicationUser();

            if(!String.IsNullOrEmpty(User.Identity.Name))
                user = _userManager.FindByNameAsync(User.Identity.Name).Result;

            if (user != null && !String.IsNullOrEmpty(user.UserName))
            {
                user.Localization = culture;
                RouteData.Values["culture"] = user.Localization;
                _userManager.UpdateAsync(user);
            }
            else
            {
                RouteData.Values["culture"] = culture;
            }

            return RedirectToAction("index", "home");
        }

        public IActionResult Search(string topsearch = "", int page = Constants.PageDefault)
        {
            var model = new SearchViewModel
            {
                TopSearch = topsearch,
                Records = _recordService.GetRecordsForSearch(topsearch).ToPagedList(Constants.PageSizeSearch, page)
            };

            return View(model);
        }

        public IActionResult AdvancedSearch()
        {
            return View();
        }
    }
}
