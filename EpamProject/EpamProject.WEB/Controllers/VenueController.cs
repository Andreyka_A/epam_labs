﻿using Microsoft.AspNetCore.Mvc;
using EpamProject.BLL.Interfaces;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using EpamProject.WEB.Models.VenueViewModels;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using EpamProject.BLL;
using System.Linq;
using Sakura.AspNetCore;

namespace EpamProject.WEB.Controllers
{
    [Authorize(Roles = "Administrator, Moderator")]
    public class VenueController : Controller
    {
        private readonly ICityService _cityService;
        private readonly IVenueService _venueService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public VenueController(
           ICityService cityService,
           IVenueService venueService,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           RoleManager<IdentityRole> roleManager)
        {
            _roleManager = roleManager;
            _cityService = cityService;
            _venueService = venueService;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult ListVenues(int cityId, int page = Constants.PageDefault)
        {
            var data = _venueService.GetByCityId(cityId).ToList();
            if (data == null)
                return NotFound();

            var pagedData = data.ToPagedList(Constants.PageSize, page);
            return View(pagedData);
        }

        [HttpGet]
        public IActionResult AddVenue(int cityId)
        {
            var city = _cityService.GetById(cityId);
            if (city == null)
                return NotFound();

            var model = new UpdateVenueModel
            {
                City = city
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddVenue(UpdateVenueModel model)
        {
            if (ModelState.IsValid)
            {
                var currentCity = _cityService.GetById(model.City.Id);
                if (currentCity == null)
                    return NotFound();

                if (_venueService.GetByAddressForCity(currentCity.Id, model.Address) != null)
                {
                    ModelState.AddModelError("Address", "This address already exists");
                    
                    model.City = currentCity;
                    return View(model);
                }

                var data = new Venue
                {
                    Name = model.Name,
                    Address = model.Address,
                    CityId = currentCity.Id
                };
                
                _venueService.Create(data);
                return RedirectToAction("listvenues", "venue", new { cityId = data.CityId });
            }
            else
            {
                var city = _cityService.GetById(model.City.Id);
                if (city == null)
                    return NotFound();

                var data = new UpdateVenueModel
                {
                    City = city
                };

                return View(data);
            }
        }

        [HttpGet]
        public IActionResult EditVenue(int venueId)
        {
            var currentVenue = _venueService.GetById(venueId);
            if (currentVenue == null)
                return NotFound();

            var data = new UpdateVenueModel
            {
                Id = currentVenue.Id,
                Name = currentVenue.Name,
                Address = currentVenue.Address,
                City = currentVenue.City
            };

            return View(data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult EditVenue(UpdateVenueModel model)
        {
            if (ModelState.IsValid)
            {
                var currentCity = _cityService.GetById(model.City.Id);
                if (currentCity == null)
                    return NotFound();

                var prevVenue = _venueService.GetById(model.Id);
                if (prevVenue == null)
                    return NotFound();

                if (prevVenue.CityId != model.City.Id)
                {
                    if (_venueService.GetByAddressForCity(currentCity.Id, model.Address) != null)
                    {
                        if (_venueService.GetByNameForCity(currentCity.Id, model.Name) != null)
                        {
                            ModelState.AddModelError("Name", "This name and address already exists");
                            ModelState.AddModelError("Address", "This name and address already exists");

                            model.City = currentCity;
                            return View(model);
                        }
                    }
                } else
                {
                    if(prevVenue.Address != model.Address)
                    {
                        if (_venueService.GetByAddressForCity(currentCity.Id, model.Address) != null)
                        {
                            ModelState.AddModelError("Address", "This address already exists");

                            model.City = currentCity;
                            return View(model);
                        }
                    } else
                    {
                        if (_venueService.GetByNameForCity(currentCity.Id, model.Name) != null)
                        {
                            ModelState.AddModelError("Name", "This name and address already exists");
                            ModelState.AddModelError("Address", "This name and address already exists");

                            model.City = currentCity;
                            return View(model);
                        }
                    }
                }

                var data = new Venue
                {
                    Name = model.Name,
                    Address = model.Address,
                    CityId = currentCity.Id
                };

                _venueService.Create(data);
                return RedirectToAction("listvenues", "venue", new { cityId = data.CityId });
            }
            else
            {
                var currentCity = _cityService.GetById(model.City.Id);
                if (currentCity == null)
                    return NotFound();

                var data = new UpdateVenueModel
                {
                    City = currentCity,
                    Name = model.Name,
                    Address = model.Address
                };

                return View(data);
            }
        }

        [HttpGet]
        public IActionResult RemoveVenue(int venueId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _venueService.GetById(venueId);
            if (data == null)
                return NotFound();

            if (data.Sessions == null)
            {
                _venueService.Remove(data);
                return RedirectToAction("listvenues", "venue", new { cityId = data.CityId });
            }
            else if (data.Sessions.Count == 0)
            {
                _venueService.Remove(data);
                return RedirectToAction("listvenues", "venue", new { cityId = data.CityId });
            }
            else
                return NotFound("Deleting is not possible, there are sessions for this venue");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
