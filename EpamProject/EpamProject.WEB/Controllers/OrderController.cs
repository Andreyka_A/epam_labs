using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Identity;
using EpamProject.DAL.Entities;
using Microsoft.Extensions.Logging;
using EpamProject.BLL.Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using EpamProject.WEB.Models.HomeViewModels;
using EpamProject.DAL.Entities.Enums;
using EpamProject.WEB.Models.ManageViewModels;
using EpamProject.BLL;

namespace EpamProject.WEB.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IStringLocalizer<AccountController> _localizer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ILogger _logger;
        private readonly ISessionService _sessionService;
        private readonly IOrderService _orderService;
        private readonly ITicketService _ticketService;

        public OrderController(
           ISessionService sessionService,
           ITicketService ticketService,
           IOrderService orderService,
           UserManager<ApplicationUser> userManager,
           SignInManager<ApplicationUser> signInManager,
           RoleManager<IdentityRole> roleManager,
            ILoggerFactory loggerFactory,
           IStringLocalizer<AccountController> localizer)
        {
            _localizer = localizer;
            _ticketService = ticketService;
            _sessionService = sessionService;
            _orderService = orderService;
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _logger = loggerFactory.CreateLogger<AccountController>();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SendRequest(DetailsViewModel model)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var ticket = _ticketService.GetById(model.Ticket.Id);
            if (model == null)
                return NotFound();

            if (user.Result.Id == ticket.ApplicationUserId)
                return NotFound("The operation is not possible. You are the owner of this ticket.");

            if (_orderService.GetActiveOrderByTicketIdAndUserId(ticket.Id, user.Result.Id) != null)
                return NotFound("You have already ordered the current ticket");

            if (ticket.State == TicketState.Closed || ticket.State == TicketState.InTransit || ticket.State == TicketState.Received)
                return NotFound();

            if (ModelState.IsValid)
            {
                if(model.PurchaseMethod == PurchaseMethod.Mail && String.IsNullOrEmpty(model.Address))
                {
                    ModelState.AddModelError("Address", "The Address field is required.");
                    model.Ticket = ticket;
                    return View("../home/details", model);
                }

                var order = new Order
                {
                    ApplicationUserId = user.Result.Id,
                    TicketId = ticket.Id,
                    IsShow = true,
                    PurchaseMethod = model.PurchaseMethod,
                    State = OrderState.Expectation,
                    PhoneNumber = model.PhoneNumber,
                    Address = model.Address,
                    Comment = model.Comment
                };

                _orderService.Create(order);
                var currentOrder = _orderService.GetLastByUserId(user.Result.Id);

                ticket.State = TicketState.Expectation;
                _ticketService.Update(ticket);
                return RedirectToAction("detailsorder", "manage", new { orderId = currentOrder.Id });
            }

            model.Ticket = ticket;
            return View("../home/details", model);
        }

        public IActionResult ConfirmOrder(int orderId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var order = _orderService.GetById(orderId);
            if (order == null)
                return NotFound();

            if (order.ApplicationUserId != user.Result.Id)
                return NotFound();

            var ticket = _ticketService.GetById(order.TicketId);
            if (ticket == null)
                return NotFound();

            if (order.TicketId != ticket.Id)
                return NotFound();

            if (order.State != OrderState.InTransit)
                return NotFound();

            order.DateCompletion = DateTime.Now;
            order.State = OrderState.Received;

            ticket.DateCompletion = DateTime.Now;
            ticket.State = TicketState.Received;

            _orderService.Update(order);
            _ticketService.Update(ticket);

            return RedirectToAction("myorders", "manage");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult RejectOrder(ListCustomersViewModel model)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var order = _orderService.GetById(model.OrderId);
            if (order == null)
                return NotFound();

            var ticket = _ticketService.GetById(order.TicketId);
            if (ticket == null)
                return NotFound();

            if (ticket.ApplicationUserId != user.Result.Id)
                return NotFound();

            int count = _orderService.GetCountActiveOrdersByTicketId(ticket.Id);

            if (String.IsNullOrEmpty(model.CommentFromSeller))
            {
                ModelState.AddModelError("CommentFromSeller", "The CommentFromSeller field is required.");

                model.Ticket = ticket;
                model.Page = Constants.PageDefault;
                model.PageSize = Constants.PageSize;

                return View($"../manage/listcustomers", model);
            }

            if ((order.State == OrderState.Expectation && (ticket.IsShow && ticket.State == TicketState.Expectation)))
            {
                order.State = OrderState.Rejected;
                order.CommentFromSeller = model.CommentFromSeller;
                _orderService.Update(order);

                if (_orderService.GetCountActiveOrdersByTicketId(ticket.Id) == 0)
                {
                    ticket.State = TicketState.Open;
                    _ticketService.Update(ticket);
                }

                model.Ticket = ticket;
                model.Page = Constants.PageDefault;
                model.PageSize = Constants.PageSize;

                return View($"../manage/listcustomers", model);
            }
            else
                return NotFound();
        }

        public IActionResult Clear(int orderId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var order = _orderService.GetById(orderId);
            if (order == null)
                return NotFound();

            if (order.ApplicationUserId != user.Result.Id)
                return NotFound();

            if ((order.State == OrderState.Closed || order.State == OrderState.Received) && order.IsShow)
            {
                _orderService.Clear(order);
                return RedirectToAction("myorders", "manage");
            }
            else
                return NotFound("The operation is not possible. Your order is in the standby state, or it has not yet delivered.");
        }

        private Task<ApplicationUser> GetCurrentUserAsync() => _userManager.GetUserAsync(HttpContext.User);
    }
}