﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using EpamProject.DAL.Entities;
using EpamProject.WEB.Models.RecordViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using EpamProject.BLL.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace EpamProject.WEB.Controllers
{
    public class RecordController : Controller
    {
        IHostingEnvironment _appEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ICategoryService _categoryService;
        private readonly IEventService _eventService;
        private readonly ISessionService _sessionService;
        private readonly IOrderService _orderService;
        private readonly IRecordService _recordService;
        private readonly ITicketService _ticketService;

        public RecordController(
            IHostingEnvironment appEnvironment, 
            ICategoryService categoryService, 
            IEventService eventManager, 
            ISessionService sessionService,
            IOrderService orderService, 
            IRecordService recordService, 
            ITicketService ticketService,
             UserManager<ApplicationUser> userManager, 
             SignInManager<ApplicationUser> signInManager)
        {
            _appEnvironment = appEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
            _categoryService = categoryService;
            _eventService = eventManager;
            _sessionService = sessionService;
            _orderService = orderService;
            _recordService = recordService;
            _ticketService = ticketService;
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, Moderator")]
        public IActionResult AddRecord()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Moderator")]
        public IActionResult AddRecord(IFormFile Poster, UpdateRecordModel model)
        {
            if (ModelState.IsValid)
            {
                var newRecord = new Record();

                var currentEvent = _eventService.GetById(model.EventId);
                if (currentEvent == null)
                    return NotFound();

                var currentCategory = _categoryService.GetById(model.CategoryId);
                if (currentCategory == null)
                    return NotFound();

                if (!currentEvent.Categories.Contains(currentCategory))
                    return NotFound();

                if (Poster != null && Poster.Length > 0)
                {
                    var file = Poster;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "images\\Posters");

                    if (file.Length > 0)
                    {
                        var fileName = Guid.NewGuid().ToString() + ".jpg";
                        var path = Path.Combine(uploads, fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            file.CopyToAsync(fileStream);
                            newRecord.Name = model.Name;
                            newRecord.Description = model.Description;
                            newRecord.Poster = fileName;
                            newRecord.CategoryId = currentCategory.Id;
                        }
                    }
                }

                _recordService.Create(newRecord);
                return RedirectToAction("browse", "home", new { eventName = currentEvent.Name, categoryName = currentCategory.Name });
            }

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, Moderator")]
        public IActionResult EditRecord(int recordId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _recordService.GetById(recordId);
            if (data == null)
                return NotFound();

            var model = new UpdateRecordModel
            {
                RecordId = recordId,
                Name = data.Name,
                Description = data.Description
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Moderator")]
        public IActionResult EditRecord(IFormFile Poster, UpdateRecordModel model)
        {
            if (ModelState.IsValid)
            {
                var updateRecord = _recordService.GetById(model.RecordId);
                if (updateRecord == null)
                    return NotFound();

                var currentCategory = _categoryService.GetById(model.CategoryId);
                if (currentCategory == null)
                    return NotFound();

                if (Poster != null && Poster.Length > 0)
                {
                    var file = Poster;
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "images\\Posters");

                    if (file.Length > 0)
                    {
                        var fileName = Guid.NewGuid().ToString() + ".jpg";
                        var path = Path.Combine(uploads, fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            file.CopyToAsync(fileStream);
                            updateRecord.Poster = fileName;
                        }
                    }
                }

                updateRecord.Name = model.Name;
                updateRecord.Description = model.Description;
                updateRecord.CategoryId = currentCategory.Id;

                _recordService.Update(updateRecord);
                return RedirectToAction("item", "home", new { recordId = updateRecord.Id });
            }

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "Administrator, Moderator")]
        public IActionResult RemoveRecord(int recordId)
        {
            var user = GetCurrentUserAsync();
            if (user.Result == null)
                return RedirectToAction("logoff", "account");

            var data = _recordService.GetById(recordId);
            if (data == null)
                return NotFound();

            if (data.Sessions == null)
            {
                _recordService.Remove(data);
                return RedirectToAction("index", "home");
            }
            else if (data.Sessions.Count == 0)
            {
                _recordService.Remove(data);
                return RedirectToAction("index", "home");
            }
            else
                return NotFound("Deleting is not possible, there are sessions for this record");
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
    }
}
