﻿$(function () {
    function initialSelect(data, selector) {
        var rows = "";

        $.each(data, function (index, item) {
            rows += addOptionToSelect(item);
        });

        $(selector).append(rows);
    }

    function addOptionToSelect(data) {
        if (data.hasOwnProperty('address')) {
            return '<option value="' + data.id + '">' + (!data.name?'':data.name + ' ') + data.address + '</option>';
        } else {
            return '<option value="' + data.id + '">' + data.name + '</option>';
        }
    }

    function getVenues(cityId) {
        $.ajax({
            url: '/api/cities/getvenues/?cityId=' + cityId,
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            success: function (venues) {
                initialSelect(venues, '#VenueId');
            }
        });
    }

    (function getCities() {
        $.ajax({
            url: '/api/cities/',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            success: function (cities) {
                initialSelect(cities, '#CityId');

                if (cities[0]) {
                    getVenues($('#CityId :selected').val());
                }
            }
        });
    })();

    $('#CityId').on('change', function () {
        var cityValue = $('#CityId :selected').val();
        $("#VenueId").empty();

        getVenues(parseInt(cityValue));
    });
});