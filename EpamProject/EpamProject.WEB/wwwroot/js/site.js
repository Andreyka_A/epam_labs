﻿$(document).ready(function () {

    $('#modal_list').on('change', function () {
        if ($('#modal_list :selected').val() === '0') {
            if ($("#toggle_input").is(":hidden") === true) {
                $("#toggle_input").show(230);
            }
        }
        if ($('#modal_list :selected').val() === '1') {
            if ($("#toggle_input").is(":hidden") === false) {
                $("#toggle_input").hide(230);
            }
        }
    });

    $('#profile_edit-pass').click(function () {
        if ($("#form_toggle").is(":hidden") === false) {
            $("#form_toggle").hide(230);
        }
        if ($("#form_toggle").is(":hidden") === true) {
            $("#form_toggle").show(230);
        }
    });
});