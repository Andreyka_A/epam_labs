﻿$(function () {
    var records = [];

    $('.autocomplete').on('input', function (e) {
        $('.ui-helper-hidden-accessible').css('display', 'none');
        $.ajax({
            url: '/api/records/',
            data: { sValue: $(this).val() },
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            success: function (data) {
                records = [];
                $(data).each(function (index, elem) {
                    records.push(elem.name);
                });
            }
        });

        $(".autocomplete").autocomplete({
            source: records
        });
    });

    $.ui.autocomplete.prototype._resizeMenu = function () {
        var ul = this.menu.element;
        ul.outerWidth(this.element.outerWidth());
    };
});