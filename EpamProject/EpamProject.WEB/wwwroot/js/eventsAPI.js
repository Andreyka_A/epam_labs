﻿$(function () {
    function initialSelect(data, selector) {
        var rows = "";

        $.each(data, function (index, item) {
            rows += addOptionToSelect(item);
        });


        $(selector).append(rows);
    }

    function addOptionToSelect(data) {
        return '<option value="' + data.id + '">' + data.name + '</option>';
    }

    function getCategories(eventId) {
        $.ajax({
            url: '/api/events/getcategories/?eventId=' + eventId,
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            success: function (categories) {
                initialSelect(categories, '#CategoryId');
            }
        });
    }

    (function getEvents() {
        $.ajax({
            url: '/api/events/',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            success: function (events) {
                initialSelect(events, '#EventId');

                if (events[0]) {
                    getCategories($('#EventId :selected').val());
                }
            }
        });
    })();

    $('#EventId').on('change', function () {
        var eventValue = $('#EventId :selected').val();
        $("#CategoryId").empty();

        getCategories(parseInt(eventValue));
    });
});