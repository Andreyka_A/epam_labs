﻿$(function () {
    var pagTotal = 0;
    var pagCurrentPage = 0;
    var pagPageSize = 0;
    var pagCount = 0;

    function initialCBCities(data, selector) {
        $.each(data, function (index, item) {
            $('<div>', {
                class: 'filter_ref'
            })
                .append($('<input/>', {
                    type: 'checkbox',
                    class: 's-cities_cb',
                    value: item.id
                }))
                .append($('<b/>').text(item.name))
                .appendTo(selector);
        });
    }

    function initialCBVenues(data, selector) {
        $.each(data, function (index, item) {
            $('<div>', {
                class: 'filter_ref'
            })
                .append($('<input/>', {
                    type: 'checkbox',
                    class: 's-venues_cb',
                    value: item.id
                }))
                .append($('<b/>').text((!item.name ? "" : item.name + " / ") + item.address))
                .appendTo(selector);
        });
    }

    function initialTableSessions(data, selector) {
        var rows = "";

        $.each(data, function (index, item) {
            var date = moment(item.date, "YYYY-MM-DD HH:mm:ss");

            $('<tr/>')
                .append($('<td/>')
                    .append($('<div/>', { class: 'news_item' })
                        .append($('<div/>', { class: 'news_img_block' })
                            .append($('<img/>', { src: '/images/Posters/' + item.poster, alt: 'poster' })))))
                .append($('<td/>', { class: 'text-capitalize' }).text(item.recordName))
                .append($('<td/>', { class: 'text-capitalize' }).text(item.cityName))
                .append($('<td/>', { class: 'text-capitalize' }).text((!item.venueName ? '' : item.venueName) + ' ' + item.venueAddress))
                .append($('<td/>', { class: 'text-capitalize' })
                    .append($('<span/>').text(date.format('D') + '.' + date.format('M') + '.' + date.format('YYYY') + ' | ' + date.format('HH'))
                        .append($('<sup/>').text(date.format('mm')))))
                .append($('<td/>', { class: 'text-capitalize' }).text(item.proposals))
                .append($('<td/>', { class: 'text-capitalize' })
                    .append($('<a/>', { class: 'text-capitalize', href: $(location).attr('href').replace('advancedsearch', 'alltickets?sessionId=' + item.id) }).text('go to tickets')))
                .appendTo(selector);
        });
    }

    function startAnimationLoading(selector) {
        $(selector).find(".ajax-loading").show();
    }

    function stopAnimationLoading(selector) {
        $(selector).find(".ajax-loading").hide();
    }

    (function getCities() {
        $.ajax({
            url: '/api/cities/',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            beforeSend: function () {
                startAnimationLoading('.s-cities_block');
            },
            complete: function () {
                stopAnimationLoading('.s-cities_block');
            },
            success: function (cities) {
                initialCBCities(cities, '.s-cities_block');
            }
        });
    })();

    (function getVenues() {
        $.ajax({
            url: '/api/venues/',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            beforeSend: function () {
                startAnimationLoading('.s-venues_block');
            },
            complete: function () {
                stopAnimationLoading('.s-venues_block');
            },
            success: function (venues) {
                initialCBVenues(venues, '.s-venues_block');
            }
        });
    })();

    (function getSessions() {
        $.ajax({
            url: '/api/sessions/',
            type: 'get',
            datatype: 'json',
            contentType: "application/json",
            beforeSend: function () {
                startAnimationLoading('.tickets_list');
            },
            complete: function () {
                stopAnimationLoading('.tickets_list');
            },
            success: function (sessions, status, jqXHR) {
                initialTableSessions(sessions, '.tickets_list table');
                pagCurrentPage = parseInt(jqXHR.getResponseHeader('Pag-CurrentPage'), 10);
                pagPageSize = parseInt(jqXHR.getResponseHeader('Pag-PageSize'), 10);
                pagCount = parseInt(jqXHR.getResponseHeader('Pag-Count'), 10);
                $('#page-selection').bootpag({ total: calculatePageTotal() });
            }
        });
    })();

    function getRequest(currentTarget) {
        var request = '';

        var recordName = $('#s-autocomplete').val();

        var citiesCb = $('.s-cities_cb:checkbox:checked');
        var venuesCb = $('.s-venues_cb:checkbox:checked');

        var date = $('#s-daterange').val().replace(';', 'T');
        var dateFrom = date.split(' - ')[0];
        var dateTo = date.split(' - ')[1];

        var $currentTarget = $(currentTarget);
        var valueTarget = $currentTarget.attr('value');

        if (valueTarget) {
            if (valueTarget.toUpperCase() === 'ASC' || valueTarget.toUpperCase() === 'DESC') {
                var orderBy = $currentTarget.text();

                request += '&sort=' + valueTarget + '&orderBy=' + orderBy;

                if (valueTarget.toUpperCase() === 'ASC')
                    $currentTarget.attr('value', 'desc');
                else
                    $currentTarget.attr('value', 'asc');
            }
        }

        if (recordName) {
            request += '&recordName=' + recordName;
        }

        if (citiesCb) {
            $.each(citiesCb, function (index, item) {
                request += '&citiesIds=' + $(item).attr('value');
            });
        }

        if (venuesCb) {
            $.each(venuesCb, function (index, item) {
                request += '&venuesIds=' + $(item).attr('value');
            });
        }

        if (dateFrom) {
            request += '&dateFrom=' + dateFrom;
        }

        if (dateTo) {
            request += '&dateTo=' + dateTo;
        }

        if (pagCurrentPage) {
            request += '&page=' + pagCurrentPage;
        }

        return request.substring(1);
    }

    function sendRequest(request) {
        $.ajax({
            url: window.location.origin + '/api/sessions/find?' + request,
            type: 'get',
            beforeSend: function () {
                $('.tickets_list table td').remove();
                startAnimationLoading('.tickets_list');
            },
            complete: function () {
                stopAnimationLoading('.tickets_list');
            },
            success: function (sessions, status, jqXHR) {
                pagCurrentPage = parseInt(jqXHR.getResponseHeader('Pag-CurrentPage'), 10);
                pagPageSize = parseInt(jqXHR.getResponseHeader('Pag-PageSize'), 10);
                pagCount = parseInt(jqXHR.getResponseHeader('Pag-Count'), 10);
                $('#page-selection').bootpag({ total: calculatePageTotal() });

                initialTableSessions(sessions, '.tickets_list table');
            }
        });

    }

    var searchTimerId;
    $('#s-autocomplete').on('input', function () {
        window.clearTimeout(searchTimerId);

        searchTimerId = setTimeout(function () {
            var request = getRequest();
            sendRequest(request);
        }, 2000);
    });
    $(document).on('click', ".ui-menu-item", function () {
        var request = getRequest();
        sendRequest(request);
    });

    $(document).on('change', ".s-cities_cb", function () {
        var request = getRequest();
        sendRequest(request);
    });
    $(document).on('change', ".s-venues_cb", function () {
        var request = getRequest();
        sendRequest(request);
    });
    $('#s-daterange').on('focus', function () {
        $(this).daterangepicker({
            locale: {
                "format": 'YYYY-MM-DD'
            },
            startDate: Date.now,
            endDate: Date.now
        });

        $(this).on('apply.daterangepicker', function () {
            var request = getRequest();
            sendRequest(request);
        });
        $(this).on('cancel.daterangepicker', function () {
            $(this).attr('value', '');
            $(this).val('');

            var request = getRequest();
            sendRequest(request);
        });
    });
    $('#s-name_sort').on('click', function () {
        var request = getRequest(this);
        sendRequest(request);
    });
    $('#s-date_sort').on('click', function () {
        var request = getRequest(this);
        sendRequest(request);
    });

    $('#page-selection').bootpag({
        total: 0,
        maxVisible: 3
    }).on("page", function (event, num) {
        pagCurrentPage = num;

        var request = getRequest(this);
        sendRequest(request);
    });

    function calculatePageTotal() {
        return Math.ceil(pagCount / pagPageSize);
    }
});