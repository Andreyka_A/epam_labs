﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace EpamProject.WEB.Services
{
    public class AcceptParameter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new Param
            {
                Name = "Accept",
                In = "header",
                Type = "string",
                Required = false
            });
        }
    }
}
