﻿using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;

namespace EpamProject.WEB.Services
{
    public class Param : IParameter
    {
        public string Description { get; set; }

        public Dictionary<string, object> Extensions { get; }

        public string In { get; set; }

        public string Name { get; set; }

        public bool Required { get; set; }

        public string Type { get; set; }
    }
}
