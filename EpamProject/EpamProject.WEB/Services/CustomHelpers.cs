﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using EpamProject.BLL;

namespace EpamProject.WEB.Services
{
    public static class CustomHelpers
    {
        public static string FirstLetterToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Input string(for Text Capitalize) can not be empty!");

            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        internal static IEnumerable<T> ToPagedList<T>(HttpResponse response, IEnumerable<T> data, int page = 0)
        {
            response.Headers.Add("Pag-Count", data.Count().ToString());
            response.Headers.Add("Pag-PageSize", Constants.PageSize.ToString());
            response.Headers.Add("Pag-CurrentPage", Constants.PageDefault.ToString());

            return data.Skip((page - 1) * Constants.PageSize).Take(Constants.PageSize).ToList();
        }
    }
}
