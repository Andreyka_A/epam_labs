﻿using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;

namespace EpamProject.BLL.Services
{
    public class VenueService : IVenueService
    {
        IUnitOfWork _context;

        public VenueService(IUnitOfWork context)
        {
            _context = context;
        }

        public void Create(string venueName)
        {
            _context.Venues.Add(new Venue { Name = venueName });
            _context.SaveChanges();
        }

        public Venue GetById(int id)
        {
            return _context.Venues.GetById(id);
        }

        public IEnumerable<Venue> GetAll()
        {
            return _context.Venues.GetAll();
        }

        public Venue GetByName(string venueName)
        {
            return GetAll().Where(e => e.Name.ToUpper() == venueName.ToUpper()).FirstOrDefault();
        }

        public Venue GetByAddress(string venueAddress)
        {
            return GetAll().Where(e => e.Address.ToUpper() == venueAddress.ToUpper()).FirstOrDefault();
        }

        public Venue GetByNameForCity(int cityId, string venueName)
        {
            var city = _context.Cities.GetById(cityId);

            if (city == null)
                return null;

            return city.Venues.Where(e => (string.IsNullOrEmpty(e.Name) ? "" : e.Name.ToUpper()) == (string.IsNullOrEmpty(venueName) ? "" : venueName.ToUpper())).FirstOrDefault();
        }

        public Venue GetByAddressForCity(int cityId, string venueAddress)
        {
            var city = _context.Cities.GetById(cityId);

            if (city == null)
                return null;

            return city.Venues.Where(e => e.Address.ToUpper() == venueAddress.ToUpper()).FirstOrDefault();
        }

        public void Create(Venue model)
        {
            _context.Venues.Add(model);
            _context.SaveChanges();
        }

        public void Update(Venue model)
        {
            _context.Venues.Update(model);
            _context.SaveChanges();
        }

        public void Remove(Venue model)
        {
            _context.Venues.Remove(model);
            _context.SaveChanges();
        }

        public IEnumerable<Venue> GetByCityId(int cityId)
        {
            return GetAll().Where(e => e.CityId == cityId);
        }
    }
}
