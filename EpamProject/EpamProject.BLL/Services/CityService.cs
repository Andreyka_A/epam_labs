﻿using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Services
{
    public class CityService : ICityService
    {
        IUnitOfWork _context;

        public CityService(IUnitOfWork context)
        {
            _context = context;
        }

        public void Create(string cityName)
        {
            _context.Cities.Add(new City { Name = cityName });
            _context.SaveChanges();
        }

        public City GetById(int id)
        {
            return _context.Cities.GetById(id);
        }

        public IEnumerable<City> GetAll()
        {
            return _context.Cities.GetAll();
        }

        public City GetByName(string cityName)
        {
            return GetAll().Where(e => e.Name.ToUpper() == cityName.ToUpper()).FirstOrDefault();
        }

        public void Create(City model)
        {
            _context.Cities.Add(model);
            _context.SaveChanges();
        }

        public void Update(City model)
        {
            _context.Cities.Update(model);
            _context.SaveChanges();
        }

        public void Remove(City model)
        {
            _context.Cities.Remove(model);
            _context.SaveChanges();
        }

        public City Find(Func<City, bool> predicate)
        {
            return GetAll().Where(predicate).FirstOrDefault();
        }
    }
}
