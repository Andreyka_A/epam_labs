﻿using System.Collections.Generic;
using System.Linq;
using EpamProject.DAL.Entities;
using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Interfaces;

namespace EpamProject.BLL.Services
{
    public class EventService : IEventService
    {
        IUnitOfWork _context;

        public EventService(IUnitOfWork context)
        {
            _context = context;
        }

        public void Create(string eventName)
        {
            _context.Events.Add(new Event { Name = eventName });
            _context.SaveChanges();
        }

        public void AddRange(params Event[] entities)
        {
            _context.Events.AddRange(entities);
            _context.SaveChanges();
        }

        public Event GetById(int id)
        {
            return _context.Events.GetById(id);
        }

        public IEnumerable<Event> GetAll()
        {
            return _context.Events.GetAll();
        }

        public Event GetByName(string name)
        {
            return GetAll().Where(e => e.Name.ToUpper() == name.ToUpper()).FirstOrDefault();
        }
    }
}
