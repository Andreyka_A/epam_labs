﻿using EpamProject.BLL.Entities.ViewModels;
using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Entities.Enums;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Services
{
    public class SessionService : ISessionService
    {
        IUnitOfWork _context;

        public SessionService(IUnitOfWork context)
        {
            _context = context;
        }

        public IEnumerable<Session> GetSessionsByRecord(int recordId)
        {
           return _context.Sessions.GetAll().Where(r => r.RecordId == recordId);
        }

        public Session GetById(int id)
        {
            return _context.Sessions.GetById(id);
        }

        public IEnumerable<Session> GetAll()
        {
            return _context.Sessions.GetAll();
        }

        public ItemModel GetSessionsByRecordWithProposals(int recordId)
        {
            var model = new ItemModel
            {
                Record = _context.Records.GetById(recordId)
            };

            model.Items = GetAll()
                .Where(r => r.RecordId == recordId)
                    .Select(x => new SessionModel
                    {
                        Session = x,
                        Proposals = x.Tickets.Where(t => t.IsShow && (t.State == TicketState.Open || t.State == TicketState.Expectation)).Count()
                    }).ToList();

            return model;
        }

        public void Create(Session model)
        {
            _context.Sessions.Add(model);
            _context.SaveChanges();
        }

        public void Update(Session model)
        {
            _context.Sessions.Update(model);
            _context.SaveChanges();
        }

        public void Remove(Session model)
        {
            _context.Sessions.Remove(model);
            _context.SaveChanges();
        }
    }
}
