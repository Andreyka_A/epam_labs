﻿using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using EpamProject.DAL.Entities;
using EpamProject.BLL.Interfaces;

namespace EpamProject.BLL.Services
{
    public class CategoryService : ICategoryService
    {
        IUnitOfWork _context;

        public CategoryService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public void Create(string categoryName)
        {
            _context.Categories.Add(new Category { Name = categoryName });
            _context.SaveChanges();
        }

        public Category GetById(int id)
        {
            return _context.Categories.GetById(id);
        }

        public IEnumerable<Category> GetAll()
        {
            return _context.Categories.GetAll();
        }

        public IEnumerable<Category> GetCategoriesByEvent(string eventName)
        {
            return GetAll().Where(e => e.Event.Name.ToUpper() == eventName.ToUpper());
        }
    }
}
