﻿using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Entities.Enums;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Services
{
    public class TicketService : ITicketService
    {
        IUnitOfWork _context;

        public TicketService(IUnitOfWork context)
        {
            _context = context;
        }

        public IEnumerable<Ticket> GetTicketsBySessionId(int sessionId)
        {
            return GetAllActive().Where(s => s.SessionId == sessionId);
        }

        public IEnumerable<Ticket> GetTicketsByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId);
        }

        public IEnumerable<Ticket> GetOpenTicketsByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && t.IsShow == true);
        }

        public IEnumerable<Ticket> GetHideTicketsByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && t.IsShow == false);
        }

        public IEnumerable<Ticket> GetActiveTicketsByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && (t.IsShow == true && t.State == TicketState.Expectation || t.State == TicketState.Open));
        }

        public Ticket GetById(int id)
        {
            return _context.Tickets.GetById(id);
        }

        public Ticket GetActive(int id)
        {
            return GetAllActive().FirstOrDefault(t => t.Id == id);
        }

        public Ticket GetLastByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId).OrderBy(i => i.Id).Last();
        }

        public IEnumerable<Ticket> GetAll()
        {
            return _context.Tickets.GetAll();
        }

        public IEnumerable<Ticket> GetAllActive()
        {
            return GetAll().Where(t => t.IsShow && (t.State == TicketState.Open || t.State == TicketState.Expectation));
        }

        public int GetCountBySession(int sessionId)
        {
            return GetAll().Where(o => o.SessionId == sessionId).Count();
        }

        void ITicketService.Create(Ticket ticket)
        {
            _context.Tickets.Add(ticket);
            _context.SaveChanges();
        }

        public void Update(Ticket ticket)
        {
            _context.Tickets.Update(ticket);
            _context.SaveChanges();
        }

        public void Close(Ticket ticket)
        {
            ticket.State = TicketState.Closed;
            Update(ticket);
            _context.SaveChanges();
        }

        public void Clear(Ticket ticket)
        {
            ticket.IsShow = false;
            Update(ticket);
            _context.SaveChanges();
        }

        public IEnumerable<Ticket> GetAllByUserId(string userId)
        {
            return GetAll().Where(u => u.ApplicationUserId == userId);
        }

        public int GetCountTotalTicketsByUserId(string userId)
        {
            return GetAllByUserId(userId).Count();
        }
        public int GetCountActiveTicketsByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && ((t.State == TicketState.Open || t.State == TicketState.Expectation))).Count();
        }
        public int GetCountSoldTicketsByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && ((t.State == TicketState.Closed || t.State == TicketState.Received))).Count();
        }
    }
}
