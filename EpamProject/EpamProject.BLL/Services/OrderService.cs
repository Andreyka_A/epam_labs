﻿using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Entities.Enums;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Services
{
    public class OrderService : IOrderService
    {
        IUnitOfWork _context;

        public OrderService(IUnitOfWork context)
        {
            _context = context;
        }

        public IEnumerable<Order> GetOrdersByUserId(string userId)
        {
            return GetAll().Where(m => m.ApplicationUserId == userId);
        }

        public Order GetById(int id)
        {
            return GetAll().FirstOrDefault(o => o.Id == id);
        }

        public IEnumerable<Order> GetAll()
        {
            return _context.Orders.GetAll();
        }

        void IOrderService.Create(Order order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
        }

        public void Update(Order order)
        {
            _context.Orders.Update(order);
            _context.SaveChanges();
        }

        public Order GetLastByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId).OrderBy(i => i.Id).Last();
        }

        public IEnumerable<Order> GetOpenOrdersByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && t.IsShow == true);
        }

        public IEnumerable<Order> GetHideOrdersByUserId(string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && t.IsShow == false);
        }

        public void Close(Order order)
        {
            order.State = OrderState.Closed;
            Update(order);
            _context.SaveChanges();
        }

        public void Clear(Order order)
        {
            order.IsShow = false;
            Update(order);
            _context.SaveChanges();
        }

        public int GetCountActiveOrdersByTicketId(int ticketId)
        {
            return GetAll().Where(t => t.TicketId == ticketId && (t.State == OrderState.Expectation)).Count();
        }

        public Order GetActiveOrderByTicketIdAndUserId(int ticketId, string userId)
        {
            return GetAll().Where(t => t.ApplicationUserId == userId && t.TicketId == ticketId).FirstOrDefault();
        }
    }
}
