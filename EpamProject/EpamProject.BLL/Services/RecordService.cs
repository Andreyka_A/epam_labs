﻿using EpamProject.BLL.Interfaces;
using EpamProject.DAL.Entities;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Services
{
    public class RecordService : IRecordService
    {
        IUnitOfWork _context;

        public RecordService(IUnitOfWork context)
        {
            _context = context;
        }

        public Record GetById(int id)
        {
            return _context.Records.GetById(id);
        }

        public IEnumerable<Record> GetAll()
        {
            return _context.Records.GetAll();
        }

        public IEnumerable<Record> GetRecordsByEvent(string eventName)
        {
            return GetAll().Where(e => e.Category.Event.Name.ToUpper() == eventName.ToUpper());
        }

        public IEnumerable<Record> GetRecordsByEvent(string eventName, string categoryName)
        {
            return GetAll().Where(e => e.Category.Event.Name.ToUpper() == eventName.ToUpper() && e.Category.Name.ToUpper() == categoryName.ToUpper());
        }

        public IEnumerable<Record> GetRecordsForSearch(string topsearch)
        {
            return GetAll().Where(x => x.Name.Contains(topsearch));
        }

        public void Create(Record model)
        {
            _context.Records.Add(model);
            _context.SaveChanges();
        }

        public void Update(Record model)
        {
            _context.Records.Update(model);
            _context.SaveChanges();
        }

        public void Remove(Record model)
        {
            _context.Records.Remove(model);
            _context.SaveChanges();
        }
    }
}