﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;

namespace EpamProject.BLL.Entities.ViewModels
{
    public class ItemModel
    {
        public Record Record { get; set; }
        public int PageSize { get; set; }
        public int Page { get; set; }

        public ICollection<SessionModel> Items { get; set; }
    }
}
