﻿using EpamProject.DAL.Entities;

namespace EpamProject.BLL.Entities.ViewModels
{
    public class SessionModel
    {
        public Session Session { get; set; }
        public int Proposals { get; set; }
    }
}
