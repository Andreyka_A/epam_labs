﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;

namespace EpamProject.BLL.Entities.ViewModels
{
    public class IndexModel
    {
        public string Event { get; set; }
        public ICollection<Record> Records { get; set; }
    }
}
