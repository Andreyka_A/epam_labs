﻿namespace EpamProject.BLL
{
    public static class Constants
    {
        public const int PageSize = 10;
        public const int PageSizeSearch = 10;
        public const int PageDefault = 1;
        public const int LimitOutputSearch = 10;
    }
}
