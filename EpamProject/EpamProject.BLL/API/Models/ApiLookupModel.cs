﻿namespace EpamProject.BLL.API.Models
{
    public class ApiLookupModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
