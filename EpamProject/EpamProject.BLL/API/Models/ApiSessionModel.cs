﻿using System;

namespace EpamProject.BLL.API.Models
{
    public class ApiSessionModel
    {
        public int Id { get; set; }
        public string Poster { get; set; }
        public DateTime Date { get; set; }
        public string RecordName { get; set; }
        public string CityName { get; set; }
        public string VenueName { get; set; }
        public string VenueAddress { get; set; }
        public int Proposals { get; set; }

        public int RecordId { get; set; }
        public int CityId { get; set; }
        public int VenueId { get; set; }
    }
}
