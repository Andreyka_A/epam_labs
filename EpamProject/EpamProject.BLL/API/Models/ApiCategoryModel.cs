﻿namespace EpamProject.BLL.API.Models
{
    public class ApiCategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EventId { get; set; }
    }
}
