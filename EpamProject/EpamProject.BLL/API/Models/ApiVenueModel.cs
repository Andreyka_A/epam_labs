﻿namespace EpamProject.BLL.API.Models
{
    public class ApiVenueModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
    }
}