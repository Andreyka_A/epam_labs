﻿using EpamProject.BLL.API.Interfaces;
using System.Collections.Generic;
using System.Linq;
using EpamProject.BLL.API.Models;
using EpamProject.DAL.Interfaces;

namespace EpamProject.BLL.API.Services
{
    public class ApiVenueService : IApiVenueService
    {
        IUnitOfWork _context;

        public ApiVenueService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public IEnumerable<ApiVenueModel> GetAll()
        {
            IEnumerable<ApiVenueModel> items = _context.Venues.GetAll()
                .Select(x => new ApiVenueModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Address = x.Address,
                    CityId = x.CityId
                });

            return items;
        }
    }
}
