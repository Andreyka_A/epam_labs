﻿using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL.API.Models;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.API.Services
{
    public class ApiEventService : IApiEventService
    {
        IUnitOfWork _context;

        public ApiEventService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public IEnumerable<ApiLookupModel> GetAll()
        {
            IEnumerable<ApiLookupModel> items = _context.Events.GetAll()
                .Select(x => new ApiLookupModel
                {
                    Id = x.Id,
                    Name = x.Name
                });

            return items;
        }

        public IEnumerable<ApiCategoryModel> GetCategories(int eventId)
        {
            IEnumerable<ApiCategoryModel> items = _context.Categories.GetAll()
                .Where(x => x.EventId == eventId)
                .Select(x => new ApiCategoryModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    EventId = x.EventId
                });

            return items;
        }
    }
}
