﻿using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL.API.Models;
using EpamProject.DAL.Entities.Enums;
using EpamProject.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.API.Services
{
    public class ApiSessionService : IApiSessionService
    {
        IUnitOfWork _context;

        public ApiSessionService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public IEnumerable<ApiSessionModel> GetAll()
        {
            IEnumerable<ApiSessionModel> items = _context.Sessions.GetAll()
                .Select(x => new ApiSessionModel
                {
                    Id = x.Id,
                    Poster = x.Poster,
                    CityName = x.Venue.City.Name,
                    CityId = x.Venue.CityId,
                    RecordName = x.Record.Name,
                    RecordId = x.RecordId,
                    VenueName = x.Venue.Name,
                    VenueAddress = x.Venue.Address,
                    VenueId = x.VenueId,
                    Date = x.Date,
                    Proposals = x.Tickets.Where(t => t.IsShow && (t.State == TicketState.Open || t.State == TicketState.Expectation)).Count()
                });

            return items;
        }

        public IEnumerable<ApiSessionModel> FindByCitiesIds(IEnumerable<ApiSessionModel> sessions, List<int> citiesIds)
        {
            return sessions.Where(x => citiesIds.Contains(x.CityId));
        }

        public IEnumerable<ApiSessionModel> FindByDateFrom(IEnumerable<ApiSessionModel> sessions, DateTime dateFrom)
        {
            return sessions.Where(x => x.Date >= dateFrom);
        }

        public IEnumerable<ApiSessionModel> FindByDateRange(IEnumerable<ApiSessionModel> sessions, DateTime dateFrom, DateTime dateTo)
        {
            return sessions.Where(x => x.Date >= dateFrom && x.Date <= dateTo);
        }

        public IEnumerable<ApiSessionModel> FindByDateTo(IEnumerable<ApiSessionModel> sessions, DateTime dateTo)
        {
            return sessions.Where(x => x.Date <= dateTo);
        }

        public IEnumerable<ApiSessionModel> FindByRecordName(IEnumerable<ApiSessionModel> sessions, string recordName)
        {
            return sessions.Where(x => x.RecordName.ToUpper().Contains(recordName.ToUpper()));
        }

        public IEnumerable<ApiSessionModel> FindByVenuesIds(IEnumerable<ApiSessionModel> sessions, List<int> venuesIds)
        {
            return sessions.Where(x => venuesIds.Contains(x.VenueId));
        }

        public IEnumerable<ApiSessionModel> Sort(IEnumerable<ApiSessionModel> sessions, string orderBy)
        {
            if (orderBy.ToUpper() == "DATE")
                return sessions.OrderBy(x => x.Date);
            else if (orderBy.ToUpper() == "NAME")
                return sessions.OrderBy(x => x.RecordName);
            else
                return null;
        }

        public IEnumerable<ApiSessionModel> SortDesc(IEnumerable<ApiSessionModel> sessions, string orderBy)
        {
            if (orderBy.ToUpper() == "DATE")
                return sessions.OrderByDescending(x => x.Date);
            else if (orderBy.ToUpper() == "NAME")
                return sessions.OrderByDescending(x => x.RecordName);
            else
                return null;
        }
    }
}
