﻿using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL.API.Models;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.API.Services
{
    public class ApiCityService : IApiCityService
    {
        IUnitOfWork _context;

        public ApiCityService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public IEnumerable<ApiLookupModel> GetAll()
        {
            IEnumerable<ApiLookupModel> items = _context.Cities.GetAll()
                .Select(x => new ApiLookupModel
                {
                    Id = x.Id,
                    Name = x.Name
                });

            return items;
        }

        public IEnumerable<ApiVenueModel> GetVenues(int cityId)
        {
            IEnumerable<ApiVenueModel> items = _context.Venues.GetAll()
                .Where(x => x.CityId == cityId)
                .Select(x => new ApiVenueModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Address = x.Address,
                    CityId = x.CityId
                });

            return items;
        }
    }
}
