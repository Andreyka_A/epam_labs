﻿using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL.API.Models;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.API.Services
{
    public class ApiCategoryService : IApiCategoryService
    {
        IUnitOfWork _context;

        public ApiCategoryService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public IEnumerable<ApiCategoryModel> GetAll()
        {
            IEnumerable<ApiCategoryModel> items= _context.Categories.GetAll()
                .Select(x => new ApiCategoryModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    EventId = x.EventId
                });

            return items;
        }
    }
}
