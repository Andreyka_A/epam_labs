﻿using EpamProject.BLL.API.Interfaces;
using EpamProject.BLL.API.Models;
using EpamProject.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.API.Services
{
    public class ApiRecordService : IApiRecordService
    {
        IUnitOfWork _context;

        public ApiRecordService(IUnitOfWork entitiesRepository)
        {
            _context = entitiesRepository;
        }

        public IEnumerable<ApiLookupModel> GetNameRecordsToSearch(string filterVal, int limit)
        {
            IEnumerable<ApiLookupModel> items = _context.Records.GetAll()
                .Select(x => new ApiLookupModel
                {
                    Id = x.Id,
                    Name = x.Name
                });

            if (filterVal == null)
                items = items.Take(limit).ToList();
            else
                items = items.Where(x => x.Name.ToUpper().Contains(filterVal.ToUpper())).Take(limit);

            return items;
        }
    }
}
