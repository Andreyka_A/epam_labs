﻿using EpamProject.BLL.API.Models;
using System.Collections.Generic;

namespace EpamProject.BLL.API.Interfaces
{
    public interface IApiCityService
    {
        IEnumerable<ApiLookupModel> GetAll();
        IEnumerable<ApiVenueModel> GetVenues(int cityId);
    }
}
