﻿using EpamProject.BLL.API.Models;
using System.Collections.Generic;
using System;

namespace EpamProject.BLL.API.Interfaces
{
    public interface IApiSessionService
    {
        IEnumerable<ApiSessionModel> GetAll();
        IEnumerable<ApiSessionModel> FindByRecordName(IEnumerable<ApiSessionModel> sessions, string recordName);
        IEnumerable<ApiSessionModel> FindByDateRange(IEnumerable<ApiSessionModel> sessions, DateTime dateFrom, DateTime dateTo);
        IEnumerable<ApiSessionModel> FindByDateFrom(IEnumerable<ApiSessionModel> sessions, DateTime dateFrom);
        IEnumerable<ApiSessionModel> FindByDateTo(IEnumerable<ApiSessionModel> sessions, DateTime dateTo);
        IEnumerable<ApiSessionModel> FindByCitiesIds(IEnumerable<ApiSessionModel> sessions, List<int> citiesIds);
        IEnumerable<ApiSessionModel> FindByVenuesIds(IEnumerable<ApiSessionModel> sessions, List<int> venuesIds);
        IEnumerable<ApiSessionModel> Sort(IEnumerable<ApiSessionModel> sessions, string orderBy);
        IEnumerable<ApiSessionModel> SortDesc(IEnumerable<ApiSessionModel> sessions, string orderBy);
    }
}
