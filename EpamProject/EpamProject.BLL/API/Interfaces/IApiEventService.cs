﻿using EpamProject.BLL.API.Models;
using System.Collections.Generic;

namespace EpamProject.BLL.API.Interfaces
{
    public interface IApiEventService
    {
        IEnumerable<ApiLookupModel> GetAll();
        IEnumerable<ApiCategoryModel> GetCategories(int eventId);
    }
}
