﻿using EpamProject.BLL.API.Models;
using System.Collections.Generic;

namespace EpamProject.BLL.API.Interfaces
{
    public interface IApiVenueService
    {
        IEnumerable<ApiVenueModel> GetAll();
    }
}
