﻿using EpamProject.BLL.API.Models;
using System.Collections.Generic;

namespace EpamProject.BLL.API.Interfaces
{
    public interface IApiRecordService
    {
        IEnumerable<ApiLookupModel> GetNameRecordsToSearch(string filterVal, int limit);
    }
}
