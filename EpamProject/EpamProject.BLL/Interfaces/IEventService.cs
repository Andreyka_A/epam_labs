﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface IEventService
    {
        void Create(string eventName);
        void AddRange(params Event[] entities);
        Event GetById(int id);
        Event GetByName(string name);
        IEnumerable<Event> GetAll();
    }
}
