﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface ICategoryService
    {
        void Create(string categoryName);
        Category GetById(int id);
        IEnumerable<Category> GetCategoriesByEvent(string eventName);
        IEnumerable<Category> GetAll();
    }
}
