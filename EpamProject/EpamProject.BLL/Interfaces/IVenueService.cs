﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface IVenueService
    {
        void Create(string venueName);
        Venue GetById(int id);
        Venue GetByName(string venueName);
        void Create(Venue model);
        void Update(Venue model);
        void Remove(Venue model);
        IEnumerable<Venue> GetAll();
        Venue GetByAddress(string venueAddress);
        Venue GetByNameForCity(int cityId, string venueName);
        Venue GetByAddressForCity(int cityId, string venueAddress);
        IEnumerable<Venue> GetByCityId(int cityId);
    }
}
