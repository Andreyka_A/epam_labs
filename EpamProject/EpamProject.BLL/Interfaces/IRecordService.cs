﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface IRecordService
    {
        IEnumerable<Record> GetAll();
        Record GetById(int id);
        void Create(Record model);
        void Update(Record model);
        void Remove(Record model);
        IEnumerable<Record> GetRecordsByEvent(string eventName);
        IEnumerable<Record> GetRecordsByEvent(string eventName, string categoryName);
        IEnumerable<Record> GetRecordsForSearch(string topsearch);
    }
}
