﻿using EpamProject.BLL.Entities.ViewModels;
using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface ISessionService
    {
        void Create(Session model);
        void Remove(Session model);
        void Update(Session model);
        IEnumerable<Session> GetSessionsByRecord(int recordId);
        Session GetById(int id);
        ItemModel GetSessionsByRecordWithProposals(int recordId);
        IEnumerable<Session> GetAll();
    }
}
