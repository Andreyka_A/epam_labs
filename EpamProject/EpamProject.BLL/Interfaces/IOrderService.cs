﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface IOrderService
    {
        void Create(Order order);
        IEnumerable<Order> GetOrdersByUserId(string userId);
        IEnumerable<Order> GetOpenOrdersByUserId(string userId);
        IEnumerable<Order> GetHideOrdersByUserId(string userId);
        Order GetActiveOrderByTicketIdAndUserId(int ticketId, string userId);
        IEnumerable<Order> GetAll();
        Order GetById(int id);
        void Update(Order order);
        Order GetLastByUserId(string userId);
        int GetCountActiveOrdersByTicketId(int ticketId);
        void Close(Order order);
        void Clear(Order order);
    }
}
