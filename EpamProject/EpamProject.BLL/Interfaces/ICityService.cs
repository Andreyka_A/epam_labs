﻿using EpamProject.DAL.Entities;
using System.Linq;
using System;
using System.Collections.Generic;

namespace EpamProject.BLL.Interfaces
{
    public interface ICityService
    {
        void Create(string cityName);
        City GetById(int id);
        City GetByName(string cityName);
        void Create(City model);
        void Update(City model);
        void Remove(City model);
        IEnumerable<City> GetAll();
        City Find(Func<City, bool> predicate);
    }
}
