﻿using EpamProject.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EpamProject.BLL.Interfaces
{
    public interface ITicketService
    {
        void Create(Ticket ticket);
        IEnumerable<Ticket> GetAll();
        IEnumerable<Ticket> GetAllByUserId(string userId);
        IEnumerable<Ticket> GetAllActive();
        IEnumerable<Ticket> GetTicketsBySessionId(int sessionId);
        IEnumerable<Ticket> GetTicketsByUserId(string userId);
        IEnumerable<Ticket> GetOpenTicketsByUserId(string userId);
        IEnumerable<Ticket> GetHideTicketsByUserId(string userId);
        Ticket GetById(int id);
        Ticket GetActive(int id);
        Ticket GetLastByUserId(string userId);
        int GetCountBySession(int offerId);
        void Update(Ticket ticket);
        void Close(Ticket ticket);
        void Clear(Ticket ticket);
        int GetCountTotalTicketsByUserId(string userId);
        int GetCountActiveTicketsByUserId(string userId);
        int GetCountSoldTicketsByUserId(string userId);
    }
}
